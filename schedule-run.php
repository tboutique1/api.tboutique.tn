<?php
// On simule l'appel à `artisan schedule:run`
use App\Mail\EvryMinuteMail;
use Illuminate\Support\Facades\Mail;

$_SERVER['argv'] = [
    'artisan',
    'schedule:run',
];

// On lance artisan
require __DIR__.'/artisan';
