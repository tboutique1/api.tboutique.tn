<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tache extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre',
        'description',
        'date',
        'heure',
        'repetition',
        'repetition_jour',
        'repetition_heure',
        'user_id',

    ];

    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            return "here";
        });
    }

    public function getTitreAttribute($value)
    {
        return ucfirst($value);
    }

    public function setTitreAttribute($value)
    {
        return $this->attributes['titre'] = strtolower($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function format()
    {
        return [
            'titre' => $this->titre,
            'description' => $this->description,
            'date' => $this->date,
            'heure' => $this->heure,
            'repetition' => $this->repetition,
            'repetition_jour' => $this->repetition_jour,
            'repetition_heure' => $this->repetition_heure,
            'id' => $this->id,
            'user_email' => $this->user->email
        ];
    }
}
