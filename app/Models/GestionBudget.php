<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GestionBudget extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'description',
        'montant',
        'action',
        'user_id',
        'date_action'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function usermeres()
    {
        return $this->belongsToMany(GestionBudget::class);
    }

    public function userfilles()
    {
        return $this->hasMany(GestionBudget::class);
    }

    public function format()
    {
        return [
            'description' => $this->description,
            'montant' => $this->montant,
            'action' => $this->action,
            'user' => $this->user->prenom,
            'date_action' => $this->date_action,
        ];
    }

}
