<?php

namespace App\Console;

use App\Console\Commands\ajoutProduitEmploiServices;
use App\Console\Commands\ajoutProduitEntreprises;
use App\Console\Commands\ajoutProduitHabillement;
use App\Console\Commands\ajoutProduitImmobilier;
use App\Console\Commands\ajoutProduitInformatiqueMultimedia;
use App\Console\Commands\ajoutProduitLoisirs;
use App\Console\Commands\ajoutProduitmaisonJardin;
use App\Console\Commands\ajoutProduitVehicules;
use App\Console\Commands\deleteOldData;
use App\Console\Commands\evryMinute;
use App\Console\Commands\firstTest;
use App\Console\Commands\newProduitviews;
use App\Console\Commands\notification;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        firstTest::class,
        evryMinute::class,
        notification::class,
        ajoutProduitInformatiqueMultimedia::class,
        ajoutProduitVehicules::class,
        ajoutProduitmaisonJardin::class,
        ajoutProduitLoisirs::class,
        ajoutProduitImmobilier::class,
        ajoutProduitEmploiServices::class,
        ajoutProduitHabillement::class,
        ajoutProduitEntreprises::class,
        newProduitviews::class,
        deleteOldData::class,
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//         $schedule->command('first:test')
  //           ->everyMinute();
//             ->runInBackground();
//             ->environments(['production']);

        $schedule->command('minute:sendmail')->everyMinute();
        $schedule->command('ajoutProduit:informatiqueMultimedia')->everyMinute();
        $schedule->command('ajoutProduit:vehicules')->everyMinute();
        $schedule->command('ajoutProduit:immobilier')->everyMinute();
        $schedule->command('ajoutProduit:maisonJardin')->everyMinute();
        $schedule->command('ajoutProduit:loisirs')->everyMinute();
        $schedule->command('ajoutProduit:emploiServices')->everyMinute();
        $schedule->command('ajoutProduit:habillement')->everyMinute();
        $schedule->command('ajoutProduit:entreprises')->everyMinute();
        $schedule->command('newProduit:views')->everyMinute();
        $schedule->command('delete:oldData')->everyMinute();
//        $schedule->command('notification:notification')->everyMinute()->runInBackground();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
