<?php

namespace App\Console\Commands;

use App\Http\Controllers\newProduit\NewProduitRepository;
use Illuminate\Console\Command;
use function App\Http\Controllers\scraping\get_http_response_code;

class newProduitviews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newProduit:views';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'newProduit views';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        set_time_limit(0);
        $newProduitRepository = new NewProduitRepository();
        $resnewProduitsearch = $newProduitRepository->searchWithCriteriaSansFormat([]);
        foreach ($resnewProduitsearch as $newProduit) {
            $totalView = $newProduit['totalview'];
            if (!$totalView) {
                $totalView = 1;
            } else {
                $totalView = $totalView + random_int(0, 5);
            }
            $newProduit->update(['totalView' => $totalView]);
        }

    }
}
