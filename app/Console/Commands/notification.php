<?php

namespace App\Console\Commands;

use App\Http\Controllers\newProduit\NewProduitSearchController;
use App\Http\Controllers\notification\NotificationSearchController;
use App\Mail\NotificationMail;
use App\Models\NewProduit;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'notification';
    private $notificationSearchController;
    private $newProduitSearchController;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NotificationSearchController $notificationSearchController,
                                NewProduitSearchController $newProduitSearchController)
    {
        $this->notificationSearchController = $notificationSearchController;
        $this->newProduitSearchController = $newProduitSearchController;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications = $this->notificationSearchController->notificationSearchSansFormat();
        foreach ($notifications as $notification) {
            $param = $notification;
            $param['sup_created_at'] = date("Y-m-d H:i:s", strtotime("now") - 3600);
            $newProduits = $this->newProduitSearchController->newProduitSearch($param);
            /** @var NewProduit $newProduit */
            foreach ($newProduits as $newProduit) {
//                var_dump($newProduit);
                Mail::to($notification['user_email'])->send(new NotificationMail($newProduit['id'], $newProduit['titre']));
            }
        }

    }
}
