<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitEntreprises extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:entreprises';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduit Entreprises';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/materiaux_et_equipement/industrie';
        $data['site']='cava';
        $data['sous_category_id']=47;
        $data['category_id']=8;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/materiaux_et_equipement/electricite_et_energie';
        $data['site']='cava';
        $data['sous_category_id']=47;
        $data['category_id']=8;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/materiaux_et_equipement/plomberie';
        $data['site']='cava';
        $data['sous_category_id']=47;
        $data['category_id']=8;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

    }
}
