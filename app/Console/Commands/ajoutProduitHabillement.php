<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitHabillement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:habillement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduitHabillement';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/mode_et_beaute/vetement';
        $data['site']='cava';
        $data['sous_category_id']=39;
        $data['category_id']=7;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/mode_et_beaute/chaussures';
        $data['site']='cava';
        $data['sous_category_id']=40;
        $data['category_id']=7;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/mode_et_beaute/accessoire';
        $data['site']='cava';
        $data['sous_category_id']=41;
        $data['category_id']=7;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/mode_et_beaute/produits_de_beaute';
        $data['site']='cava';
        $data['sous_category_id']=45;
        $data['category_id']=7;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/bebe_et_enfant/vetements';
        $data['site']='cava';
        $data['sous_category_id']=43;
        $data['category_id']=7;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/bebe_et_enfant/equipements';
        $data['site']='cava';
        $data['sous_category_id']=44;
        $data['category_id']=7;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

    }
}
