<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitInformatiqueMultimedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:informatiqueMultimedia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' ajoutProduit Informatique Multimedia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/Telephones';
        $data['site']='cava';
        $data['sous_category_id']=27;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/informatique_et_multimedia/image_et_son';
        $data['site']='cava';
        $data['sous_category_id']=28;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/informatique_et_multimedia/ordinateur';
        $data['site']='cava';
        $data['sous_category_id']=29;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/informatique_et_multimedia/lesaccessoires';
        $data['site']='cava';
        $data['sous_category_id']=30;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/loisirs_et_jeux/jeux_et_jouets';
        $data['site']='cava';
        $data['sous_category_id']=31;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/informatique_et_multimedia/tablettes';
        $data['site']='cava';
        $data['sous_category_id']=33;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/informatique_et_multimedia/televisions';
        $data['site']='cava';
        $data['sous_category_id']=34;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);
    }
}
