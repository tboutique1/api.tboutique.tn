<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    public $path = '';
    public $data = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command will generate site map for tboutique.tn';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $fileNmae = 'sitemap.xml';
            $this->path = public_path('sitemap/');
            ini_set("memory_limit", "-1");
            set_time_limit(0);
            ini_set("max_execution_time", 0);
            ignore_user_abort(true);
//file 1
            if (file_exists($this->path . $fileNmae)) {
                chmod($this->path, 0777);
                chmod($this->path . $fileNmae, 0777);
                rename($this->path . $fileNmae, $this->path . $fileNmae . 'sitemap-old' . date('D-d-M-Y h-s') . '.xml');
            }
            $sitemap = SitemapGenerator::create('https://tboutique.tn')->getSitemap();
            $produits = \App\Models\NewProduit::all()->take(49000);

            foreach ($produits as $produit) {
                $urlProduit = 'https://tboutique.tn/newProduit/view/' . $produit->getId();

                $sitemap->add(Url::create($urlProduit)
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
                    ->setPriority(0.7));
            }
            $sitemap->writeToFile($this->path . $fileNmae);
            $sitemapUrl = 'https://api.tboutique.tn/public/sitemap' . $fileNmae;



            function myCurl($url)
            {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                return $httpCode;
            }

            //site map for google
            $url = "http://www.google.com/webmasters/sitemaps/ping?sitemap=" . $sitemapUrl;
            $returnCode = myCurl($url);
            echo "<p> Google site maps hes been pinged ( return code : $returnCode).</p>";

            //site map for Bing / msn
            $url = "http://www.bing.com/ping?siteMap=" . $sitemapUrl;
            $returnCode = myCurl($url);
            echo "<p> MSN site maps hes been pinged ( return code : $returnCode).</p>";

            //site map for ASK
            $url = "http://subnissions.ask.com/ping?sitemap=" . $sitemapUrl;
            $returnCode = myCurl($url);
            echo "<p> ASK.com site maps hes been pinged ( return code : $returnCode).</p>";

//file 2
            $fileNmae2 = 'sitemap.xml';
            $this->path = public_path('sitemap/2/');
            if (file_exists($this->path . $fileNmae2)) {
                chmod($this->path, 0777);
                chmod($this->path . $fileNmae2, 0777);
                rename($this->path . $fileNmae2, $this->path . $fileNmae2 . 'sitemap2-old' . date('D-d-M-Y h-s') . '.xml');
            }
            $sitemap2 = SitemapGenerator::create('https://tboutique.tn')->getSitemap();
            $produits2 = \App\Models\NewProduit::all()->skip(49000);

            foreach ($produits2 as $produit) {
                $urlProduit = 'https://tboutique.tn/newProduit/view/' . $produit->getId();

                $sitemap2->add(Url::create($urlProduit)
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
                    ->setPriority(0.7));
            }
            $sitemap2->writeToFile($this->path . $fileNmae2);
            $sitemapUrl2 = 'https://api.tboutique.tn/public/sitemap' . $fileNmae;

            //site map for google
            $url = "http://www.google.com/webmasters/sitemaps/ping?sitemap=" . $sitemapUrl2;
            $returnCode = myCurl($url);
            echo "<p> Google site maps hes been pinged ( return code : $returnCode).</p>";

            //site map for Bing / msn
            $url = "http://www.bing.com/ping?siteMap=" . $sitemapUrl2;
            $returnCode = myCurl($url);
            echo "<p> MSN site maps hes been pinged ( return code : $returnCode).</p>";

            //site map for ASK
            $url = "http://subnissions.ask.com/ping?sitemap=" . $sitemapUrl2;
            $returnCode = myCurl($url);
            echo "<p> ASK.com site maps hes been pinged ( return code : $returnCode).</p>";

        } catch (\Exception $e) {
            Log::error($e);
        }
    }
}
