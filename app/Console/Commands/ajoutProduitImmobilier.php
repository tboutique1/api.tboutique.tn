<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitImmobilier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:immobilier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduit immobilier';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/immobilier/appartemen';
        $data['site']='cava';
        $data['sous_category_id']=1;
        $data['category_id']=1;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/immobilier/maisons_et_villas';
        $data['site']='cava';
        $data['sous_category_id']=2;
        $data['category_id']=1;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/immobilier/terrains';
        $data['site']='cava';
        $data['sous_category_id']=6;
        $data['category_id']=1;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/immobilier/locations_de_vacances';
        $data['site']='cava';
        $data['sous_category_id']=3;
        $data['category_id']=1;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/immobilier/bureaux_et_commerces';
        $data['site']='cava';
        $data['sous_category_id']=4;
        $data['category_id']=1;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/immobilier/colocation';
        $data['site']='cava';
        $data['sous_category_id']=8;
        $data['category_id']=1;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/immobilier/autres';
        $data['site']='cava';
        $data['sous_category_id']=7;
        $data['category_id']=1;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

    }
}
