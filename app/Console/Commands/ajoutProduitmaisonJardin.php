<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitmaisonJardin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:maisonJardin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduit maisonJardin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/maison_et_jardin/electromenager';
        $data['site']='cava';
        $data['sous_category_id']=17;
        $data['category_id']=3;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/maison_et_jardin/meubles';
        $data['site']='cava';
        $data['sous_category_id']=18;
        $data['category_id']=3;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/maison_et_jardin/decoration';
        $data['site']='cava';
        $data['sous_category_id']=18;
        $data['category_id']=3;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/maison_et_jardin/jardinage';
        $data['site']='cava';
        $data['sous_category_id']=19;
        $data['category_id']=3;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

    }
}
