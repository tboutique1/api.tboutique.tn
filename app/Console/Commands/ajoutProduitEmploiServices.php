<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitEmploiServices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:emploiServices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduit Emploi et Services';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/emploi_et_services/offres_emploi';
        $data['site']='cava';
        $data['sous_category_id']=35;
        $data['category_id']=6;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/emploi_et_services/demandes_emploi';
        $data['site']='cava';
        $data['sous_category_id']=36;
        $data['category_id']=6;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/emploi_et_services/service';
        $data['site']='cava';
        $data['sous_category_id']=37;
        $data['category_id']=6;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

    }
}
