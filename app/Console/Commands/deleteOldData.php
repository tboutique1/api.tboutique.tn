<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class deleteOldData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:oldData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete old data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       // $sc=new \App\Http\Controllers\scraping\Scraping();
       // $sc->deleteOldData();
    }
}
