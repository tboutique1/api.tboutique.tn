<?php

namespace App\Console\Commands;

use App\Http\Controllers\newProduit\NewProduitSearchController;
use App\Http\Controllers\notification\NotificationSearchController;
use App\Http\Controllers\tache\TacheSearchController;
use App\Mail\NotificationMail;
use App\Models\NewProduit;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class tache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tache:tache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'tache';

    /** @var TacheSearchController $tacheSearchController */
    private $tacheSearchController;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TacheSearchController $tacheSearchController)
    {
        $this->tacheSearchController = $tacheSearchController;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->tacheSearchController->tacheMail();
    }
}
