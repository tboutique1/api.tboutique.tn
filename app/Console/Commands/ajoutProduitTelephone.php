<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitTelephone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:telephone';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/Telephones';
        $data['site']='cava';
        $data['sous_category_id']=27;
        $data['category_id']=5;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);
    }
}
