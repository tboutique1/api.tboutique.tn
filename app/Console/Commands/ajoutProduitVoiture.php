<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitVoiture extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:voiture';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduit voiture';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/voitures';
        $data['site']='cava';
        $data['sous_category_id']=9;
        $data['category_id']=2;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);
    }
}
