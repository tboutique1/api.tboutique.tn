<?php

namespace App\Console\Commands;

use App\Mail\EvryMinuteMail;
use App\Mail\MailVerificationMail;
use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Mail;

class evryMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minute:sendmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'envoi mail chaque minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Mail::to("mrk19933@gmail.com")->send(new EvryMinuteMail());
    }

}
