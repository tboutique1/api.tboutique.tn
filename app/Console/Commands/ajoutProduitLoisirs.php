<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ajoutProduitLoisirs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ajoutProduit:loisirs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ajoutProduit loisirs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data['url']='https://www.cava.tn/category/vehicules_et_pieces/velos';
        $data['site']='cava';
        $data['sous_category_id']=20;
        $data['category_id']=4;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/loisirs_et_jeux/sports_et_loisirs';
        $data['site']='cava';
        $data['sous_category_id']=21;
        $data['category_id']=4;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/animaux';
        $data['site']='cava';
        $data['sous_category_id']=22;
        $data['category_id']=4;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/loisirs_et_jeux/voyages';
        $data['site']='cava';
        $data['sous_category_id']=24;
        $data['category_id']=4;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/loisirs_et_jeux/collection';
        $data['site']='cava';
        $data['sous_category_id']=25;
        $data['category_id']=4;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

        $data['url']='https://www.cava.tn/category/loisirs_et_jeux/musiques';
        $data['site']='cava';
        $data['sous_category_id']=26;
        $data['category_id']=4;
        $sc=new \App\Http\Controllers\scraping\Scraping();
        $sc->addAlldataFromCava($data);

    }
}
