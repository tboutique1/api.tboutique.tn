<?php

namespace App\Policies;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotificationPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->hasRole('admin')) {
            return true;
        }
    }

    public function index(User $user)
    {
        return ($user->hasRole('admin'));
    }

    public function store(User $user)
    {
        if ($user->societe_id) {
            return true;
        } else {
            return false;
        }
    }

    public function update(User $user, Notification $notification)
    {
        $isadmin = $user->hasRole('admin');
        $appartient = $user->id == $notification->user_id;
        return $appartient || $isadmin;
    }

    public function destroy(User $user, Notification $notification)
    {
        $isadmin = $user->hasRole('admin');
        $appartient = $user->id == $notification->user_id;
        return $appartient || $isadmin;
    }
}
