<?php

namespace App\Policies;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GestionBudgetPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
       // return ($user->id == 1 || $user->id == 2);
        return true;
    }

    public function index(User $user)
    {
        // return ($user->id == 1 || $user->id == 2);
        return true;
    }

    public function store(User $user)
    {
        // return ($user->id == 1 || $user->id == 2);
        return true;
    }
}
