<?php

namespace App\Http\Controllers\notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\historique\HistoriqueController;
use App\Http\Requests\NotificationCreateRequest;
use App\Models\Notification;
use App\Models\NotificationImages;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class NotificationController extends Controller
{
    protected $user;
    /** @var HistoriqueController */
    protected $historiqueController;
    const CONTROLLER_NAME = 'Notification';

    public function __construct(HistoriqueController $historiqueController)
    {
        $this->historiqueController = new HistoriqueController();
        if (JWTAuth::getToken()) {
            $this->user = JWTAuth::parseToken()->authenticate();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Notification::class);
        return Notification::where('societe_id', '=', Auth::user()->societe_id)->orderBy('order')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationCreateRequest $request)
    {

        $this->authorize('store', Notification::class);
            $param = $request->all();
            $param['societe_id'] = Auth::user()->societe_id;
            $param['user_id'] = Auth::user()->id;
//            return $param;
            $res = Notification::create($param);

            if ($res) {
                $this->saveHistorique('store', $request->all());

                return response()->json(['data' => $res->format(), 'message' => 'Notification cree avec succee'], 200);
            } else {
                return response()->json(['error' => 'Echec creation Notification'], 400);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        return $notification->format();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotificationCreateRequest $request, Notification $notification)
    {
        $this->authorize('update', $notification);
        $param = $request->all();
        $res = $notification->update($param);

        if ($res) {
            $this->saveHistorique('update', $param);
            return response()->json(['data' => $notification->format(), 'message' => 'Notification cree avec succee'], 200);
        } else {
            return response()->json(['error' => 'Echec creation Notification'], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        $this->authorize('destroy', $notification);
        $res = $notification->delete();
        if ($res) {
            $this->saveHistorique('destroy', $notification->id);
            return response()->json(['message' => 'Notification modifier avec succee'], 200);
        } else {
            return response()->json(['error' => 'Echec supprimer Notification'], 400);
        }

    }

    private function saveHistorique($action, $action_contenu)
    {
        $contenu = $action_contenu;
        $this->historiqueController->store(
            [
                'controller' => $this::CONTROLLER_NAME,
                'action' => $action,
                'action_contenu' => $contenu,
            ]
        );
    }
}
