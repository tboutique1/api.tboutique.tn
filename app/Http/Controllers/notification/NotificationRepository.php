<?php


namespace App\Http\Controllers\notification;


use App\Models\Notification;
use Illuminate\Database\Query\Builder;

class NotificationRepository
{
    public function searchWithCriteria($criteria)
    {
        /** @var Builder $qr */
        $qr = Notification::orderBy('id','DESC');
        foreach ($criteria as $key => $value) {
            if ($value !== null) {
                switch ($key) {
                    case 'titre':
                        $qr->where('titre', 'like', '%' . $value . '%');
                        break;
                    case 'sousCategory_id':
                        $qr->where('sous_category_id', '=', $value);
                        break;
                    case 'sous_category_id':
                        $qr->where('sous_category_id', '=', $value);
                        break;
                    case 'category_id':
                        $qr->where('category_id', '=', $value);
                        break;
                    case 'delegation_id':
                        $qr->where('delegation_id', '=', $value);
                        break;
                    case 'gouvernorat_id':
                        $qr->where('gouvernorat_id', '=', $value);
                        break;
                    case 'prix_max':
                        $qr->where('prix', '<', $value);
                        break;
                    case 'prix_min':
                        $qr->where('prix', '>', $value);
                        break;
                    case 'societe_id':
                        $qr->where('societe_id', '=', $value);
                        break;
                    case 'user_id':
                        $qr->where('user_id', '=', $value);
                        break;
                }
            }
        }
        return $qr->get()->map->format();

    }
    public function searchWithCriteriaSansFormat($criteria)
    {
        /** @var Builder $qr */
        $qr = Notification::orderBy('id','DESC');
        foreach ($criteria as $key => $value) {
            if ($value !== null) {
                switch ($key) {
                    case 'titre':
                        $qr->where('titre', 'like', '%' . $value . '%');
                        break;
                    case 'sousCategory_id':
                        $qr->where('sous_category_id', '=', $value);
                        break;
                    case 'sous_category_id':
                        $qr->where('sous_category_id', '=', $value);
                        break;
                    case 'category_id':
                        $qr->where('category_id', '=', $value);
                        break;
                    case 'delegation_id':
                        $qr->where('delegation_id', '=', $value);
                        break;
                    case 'gouvernorat_id':
                        $qr->where('gouvernorat_id', '=', $value);
                        break;
                    case 'prix_max':
                        $qr->where('prix', '<', $value);
                        break;
                    case 'prix_min':
                        $qr->where('prix', '>', $value);
                        break;
                    case 'societe_id':
                        $qr->where('societe_id', '=', $value);
                        break;
                    case 'user_id':
                        $qr->where('user_id', '=', $value);
                        break;
                }
            }
        }
        return $qr->get();

    }

}
