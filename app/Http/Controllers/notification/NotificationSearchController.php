<?php

namespace App\Http\Controllers\notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\newProduit\NewProduitSearchController;
use App\Http\Requests\NotificationCreateRequest;
use App\Mail\EvryMinuteMail;
use App\Mail\NotificationMail;
use App\Models\NewProduit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class NotificationSearchController extends Controller
{
    private $notificationRepository;
    private $newProduitSearchController;

    public function __construct(NotificationRepository $notificationRepository,
                                NewProduitSearchController $newProduitSearchController)
    {
        $this->notificationRepository = $notificationRepository;
        $this->newProduitSearchController = $newProduitSearchController;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NotificationCreateRequest $request
     * @return Illuminate\Http\Request
     */
    public function store(Request $request)
    {
        $param = $request->all();
        if (Auth::user()->id) {
            $param['user_id'] = Auth::user()->id;
        } else {
            $param['user_id'] = 0;
        }
        $notifications = $this->notificationRepository->searchWithCriteria($param);
        return $notifications;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NotificationCreateRequest $request
     * @return Illuminate\Http\Request
     */
    public function notificationSearch()
    {
        $notifications = $this->notificationRepository->searchWithCriteria([]);
        return $notifications;
    }

    public function notificationSearchSansFormat()
    {
        $notifications = $this->notificationRepository->searchWithCriteriaSansFormat([]);
        return $notifications->map->formatNotification();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NotificationCreateRequest $request
     * @return Illuminate\Http\Request
     */
    public function notificationSocieteSearch(Request $request)
    {
        $param = $request->all();
        if (Auth::user()->id) {
            $param['user_id'] = Auth::user()->id;
        } else {
            $param['user_id'] = 0;
        }
        $notifications = $this->notificationRepository->searchWithCriteria($param);
        return $notifications;
    }

    public function testNotification()
    {
        $notifications = $this->notificationSearchSansFormat();
        foreach ($notifications as $notification) {
            $param = $notification;
            $param['sup_created_at'] = date("Y-m-d H:i:s", strtotime("now") - 7200);
            $newProduits = $this->newProduitSearchController->newProduitSearch($param);
            /** @var NewProduit $newProduit */
            foreach ($newProduits as $newProduit) {
                var_dump($newProduit);
                Mail::to($notification['user_email'])->send(new NotificationMail($newProduit['id'],$newProduit['titre']));
            }
        }
        return 1;

    }

}
