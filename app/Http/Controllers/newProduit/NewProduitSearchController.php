<?php

namespace App\Http\Controllers\newProduit;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewProduitCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewProduitSearchController extends Controller
{
    private $newProduitRepository;

    public function __construct(NewProduitRepository $newProduitRepository)
    {
        $this->newProduitRepository = $newProduitRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewProduitCreateRequest $request
     * @return Illuminate\Http\Request
     */
    public function store(Request $request)
    {
        $newProduits = $this->newProduitRepository->searchWithCriteria($request->all());
        return $newProduits;
    }
    public function newProduitSearch($request)
    {
        $newProduits = $this->newProduitRepository->searchWithCriteriaSansFormat($request);
        return $newProduits->map->formatNotification();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param NewProduitCreateRequest $request
     * @return Illuminate\Http\Request
     */
    public function newProduitSocieteSearch(Request $request)
    {
        $param=$request->all();
        if(Auth::user()->societe_id){
            $param['societe_id']=Auth::user()->societe_id;
        }else{
            $param['societe_id']=0;
        }
        $newProduits = $this->newProduitRepository->searchWithCriteria($param);
        return $newProduits;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param NewProduitCreateRequest $request
     * @return Illuminate\Http\Request
     */
    public function produitUtilisateurSocieteSearch(Request $request)
    {
        $param=$request->all();
        if(Auth::user()->id){
            $param['user_id']=Auth::user()->id;
        }else{
            $param['user_id']=0;
        }
        $newProduits = $this->newProduitRepository->searchWithCriteria($param);
        return $newProduits;
    }

}
