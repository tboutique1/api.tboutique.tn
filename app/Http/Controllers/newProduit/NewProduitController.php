<?php

namespace App\Http\Controllers\newProduit;

use App\Http\Controllers\Controller;
use App\Http\Controllers\delegation\DelegationRepository;
use App\Http\Controllers\gouvernorat\GouvernoratRepository;
use App\Http\Controllers\historique\HistoriqueController;
use App\Http\Controllers\marque\MarqueRepository;
use App\Http\Controllers\modele\ModeleRepository;
use App\Http\Requests\NewProduitCreateRequest;
use App\Mail\NewDataMail;
use App\Models\NewProduit;
use App\Models\NewProduitImages;
use App\Models\Societe;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use JWTAuth;

class NewProduitController extends Controller
{
    protected $user;
    protected $newProduitId;
    /** @var HistoriqueController */
    protected $historiqueController;
    const CONTROLLER_NAME = 'NewProduit';

    public function __construct(HistoriqueController $historiqueController)
    {
        $this->historiqueController = new HistoriqueController();
        if (JWTAuth::getToken()) {
            $this->user = JWTAuth::parseToken()->authenticate();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', NewProduit::class);
        return NewProduit::where('societe_id', '=', Auth::user()->societe_id)->orderBy('order')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addNewProduitTayara(NewProduitCreateRequest $request)
    {
        $param = $request->all();
        $ModelRep = new ModeleRepository();
        if ($param['sous_category_id'] == 9) {
            $marqueRep = new MarqueRepository();
            $marque = $marqueRep->searchWithCriteria(['nomExacte' => strtolower($param['marque'])])->first();
            if ($marque) {
                $param['marque_id'] = $marque['id'];
            } else {
                $key = 'New marque ' . $param['modele'];
                Mail::to("med.riadh.kh@gmail.com")->send(new NewDataMail($key, $param['marque']));
                $param['marque_id'] = 0;
                $param['autre_marque'] = $param['marque'];
            }
            $model = $ModelRep->searchWithCriteria(['nomExacte' => strtolower($param['modele'])])->first();
            if ($model) {
                $param['modele_id'] = $model['id'];
            } else {
                $key = 'New modele ' . $param['marque'];
                Mail::to("med.riadh.kh@gmail.com")->send(new NewDataMail($key, $param['modele']));
                $param['modele_id'] = 0;
                $param['autre_modele'] = $param['modele'];
            }

        }

        if (isset($param['gouvernorat'])) {
            $gouvernoratRepository = new GouvernoratRepository();
            $gouv = $gouvernoratRepository->searchWithCriteria(['nomExacte' => trim(strtolower($param['gouvernorat']))])->first();
            if (!$gouv) {
                $gouv = false;
            }
        } else {
            $gouv = false;
        }
        if (isset($param['delegation'])) {
            $delegationRepository = new DelegationRepository();
            $del = $delegationRepository->searchWithCriteria(['nomExacte' => trim(strtolower($param['delegation']))])->first();
            if (!$del) {
                $del = false;
            }
        } else {
            $del = false;
        }
        if ($del) {
            $param['delegation_id'] = intval($del['id']);
        } else {
            $key = $param['titre'] .' '. $param['adresse'].' New delegation ' . $param['gouvernorat']  ;
            Mail::to("med.riadh.kh@gmail.com")->send(new NewDataMail($key, $param['delegation']));
            $param['delegation_id'] = 0;
            $param['autre_delegation'] = trim($param['delegation']);
        }
        if ($gouv) {
            $param['gouvernorat_id'] = intval($gouv['id']);
        } else {
            $key = $param['titre'] .' '. $param['adresse'].' New gouvernorat ' . $param['delegation']  ;
            Mail::to("med.riadh.kh@gmail.com")->send(new NewDataMail($key, $param['gouvernorat']));
            $param['gouvernorat_id'] = 0;
            $param['autre_gouvernorat'] = trim($param['gouvernorat']);

        }
        $newProduitRepository = new NewProduitRepository();

        $resnewProduitsearch = $newProduitRepository->searchWithCriteriaSansFormat(['image_path' => $param['image_path']]);
        if (count($resnewProduitsearch) == 0) {
            $res = NewProduit::create($param);
            $this->newProduitId = $res->id;

            foreach ($param['imageUrls'] as $item) {
                $paramImage['image_name'] = 'test';
                $paramImage['image_path'] = $item;
                $paramImage['new_produit_id'] = $this->newProduitId;
                NewProduitImages::create($paramImage);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewProduitCreateRequest $request)
    {

        $this->authorize('store', NewProduit::class);
        if ($request->hasFile('selectedFile')) {

            $param = $request->all();
            $param['societe_id'] = Auth::user()->societe_id;

            if ($param['societe_id'] != 1) {
                /** @var Societe $societe */
                $societe = Auth::user()->getSociete();
                $param['delegation_id'] = $societe->delegation_id;
                $param['gouvernorat_id'] = $societe->gouvernorat_id;
                $param['adresse'] = $societe->adresse;
            }
            $param['user_id'] = Auth::user()->id;
            $files = $request->file('selectedFile');
            $selectedFirstFile = null;
            if (count($files) != 0) {
                $selectedFirstFile = $files[0];
                $fileNameExtension = $selectedFirstFile->getClientOriginalName();
                $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);
                $extension = pathinfo($fileNameExtension, PATHINFO_EXTENSION);
                $fileNameUnique = $fileName . '_' . Carbon::now()->timestamp . '.' . $extension;
                $selectedFirstFile->storeAs('new_produits_images', $fileNameUnique, 'public');
                $param['image_name'] = $fileName;
                $param['image_path'] = $fileNameUnique;
            }

            unset($param['selectedFile']);
//            return $param;
            $res = NewProduit::create($param);
            $newProduitId = $res->id;
            foreach ($request->file('selectedFile') as $selectedFile) {
                $fileNameExtension = $selectedFile->getClientOriginalName();
                $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);
                $extension = pathinfo($fileNameExtension, PATHINFO_EXTENSION);
                $fileNameUnique = $fileName . '_' . Carbon::now()->timestamp . '.' . $extension;
                $selectedFile->storeAs('new_produits_images', $fileNameUnique, 'public');
                $paramImage['image_name'] = $fileName;
                $paramImage['image_path'] = $fileNameUnique;
                $paramImage['new_produit_id'] = $newProduitId;
                NewProduitImages::create($paramImage);
            }

            if ($res) {
                $this->saveHistorique('store', $request->all());

                return response()->json(['data' => $res->format(), 'message' => 'NewProduit cree avec succee'], 200);
            } else {
                return response()->json(['error' => 'Echec creation NewProduit'], 400);
            }
        } else {
            return response()->json(['error' => 'Une image est obligatoire pour ajouter un produit'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(NewProduit $newProduit)
    {
        $totalView = $newProduit['totalview'];
        if (!$totalView) {
            $totalView = 1;
        } else {
            $totalView = $totalView + 1;
        }
        $newProduit->update(['totalView' => $totalView]);
        return $newProduit->formatShow();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function produitView(NewProduit $newProduit)
    {
//        $data=$newProduit;
//        dd($newProduit);
        return view('produit.produit', ['newProduit' => $newProduit->format()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewProduitCreateRequest $request, NewProduit $newProduit)
    {
        $this->authorize('update', $newProduit);
        $param = $request->all();

        if ($request->hasFile('selectedFile')) {
            $files = $request->file('selectedFile');
            $selectedFirstFile = null;
            if (count($files) != 0) {
                $selectedFirstFile = $files[0];
                $fileNameExtension = $selectedFirstFile->getClientOriginalName();
                $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);
                $extension = pathinfo($fileNameExtension, PATHINFO_EXTENSION);
                $fileNameUnique = $fileName . '_' . Carbon::now()->timestamp . '.' . $extension;
                $selectedFirstFile->storeAs('new_produits_images', $fileNameUnique, 'public');
                $param['image_name'] = $fileName;
                $param['image_path'] = $fileNameUnique;
            }
            $newProduitImages = NewProduitImages::where('new_produit_id', '=', $newProduit->id)->get();
            foreach ($newProduitImages as $newProduitImage) {
                $newProduitImage->delete();
            }
            foreach ($request->file('selectedFile') as $selectedFile) {
                $fileNameExtension = $selectedFile->getClientOriginalName();
                $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);
                $extension = pathinfo($fileNameExtension, PATHINFO_EXTENSION);
                $fileNameUnique = $fileName . '_' . Carbon::now()->timestamp . '.' . $extension;
                $selectedFile->storeAs('new_produits_images', $fileNameUnique, 'public');
                $paramImage['image_name'] = $fileName;
                $paramImage['image_path'] = $fileNameUnique;
                $paramImage['new_produit_id'] = $newProduit->id;
                NewProduitImages::create($paramImage);
            }
        }
        unset($param['selectedFile']);
        $res = $newProduit->update($param);

        if ($res) {
            $this->saveHistorique('update', $param);
            return response()->json(['data' => $newProduit->format(), 'message' => 'NewProduit cree avec succee'], 200);
        } else {
            return response()->json(['error' => 'Echec creation NewProduit'], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewProduit $newProduit)
    {
        $this->authorize('destroy', $newProduit);
        foreach ($newProduit->newProduitImages as $newProduitImage) {
            $path = explode('/', $newProduitImage->image_path);
            if (file_exists(storage_path('app/public/new_produits_images/' . end($path)))) {
                unlink(storage_path('app/public/new_produits_images/' . end($path)));
            }
        }
        $newProduit->newProduitImages->each->delete();
        $res = $newProduit->delete();
        if ($res) {
            $this->saveHistorique('destroy', $newProduit->id);
            return response()->json(['message' => 'NewProduit modifier avec succee'], 200);
        } else {
            return response()->json(['error' => 'Echec supprimer NewProduit'], 400);
        }

    }

    private function saveHistorique($action, $action_contenu)
    {
        $contenu = $action_contenu;
        $this->historiqueController->store(
            [
                'controller' => $this::CONTROLLER_NAME,
                'action' => $action,
                'action_contenu' => $contenu,
            ]
        );
    }

}
