<?php

namespace App\Http\Controllers\gestionBudget;

use App\Http\Controllers\Controller;
use App\Models\GestionBudget;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class GestionBudgetController extends Controller
{
    public function __construct()
    {
        if (JWTAuth::getToken()) {
            $this->user = JWTAuth::parseToken()->authenticate();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->authorize('store', GestionBudget::class);
            $param = $request->all();
            $param['user_id'] = Auth::user()->id;
            unset($param['selectedFile']);
            return GestionBudget::create($param);

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Modele $modele
     * @return  modele $modele
     */
    public function getBudget()
    {
        /** @var User $user */
        $user = auth()->user();
        $resultat = [];
        /** @var GestionBudget $budget */


        $resultat['budgets'] = $user->budgets->map->format();
        $budgetArray = $budget->format();
        /*        $budgetArray=[
                  'description'=>$budget->description,
                  'montant'=>$budget->montant,
                  'action'=>$budget->action,
                  'date_action'=>$budget->date_action,
                ];*/
        $userFilles = $budget->userfilles;
        /** @var User $userFille */
        foreach ($userFilles as $userFille) {
            $budget = $user->budgets->first();
            $budgetArray['budget'] = $budget->format();
//            $budgetArray['total'] = $this->getTotal($b);

            $sousBudget[] = $budgetArray;
        }
        $sousBudget = $budget->userfilles;
/*        $sousBudgetArray = [

        ]
        return $modele;*/

    }

    protected function getTotal($gestionBudgets)
    {
        $total = 0;
        foreach ($gestionBudgets as $gestionBudget) {
            if ($gestionBudget['action'] == 1) {
                $total += $gestionBudget['montant'];
            } else {
                $total -= $gestionBudget['montant'];
            }
        }
        return $total;
    }
}
