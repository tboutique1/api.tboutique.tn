<?php


namespace App\Http\Controllers\gestionBudget;


use App\Models\GestionBudget;
use Illuminate\Database\Query\Builder;

class GestionBudgetRepository
{
    private $offset = 0;
    private $limit = 50;

    public function searchWithCriteria($criteria)
    {
        if (isset($criteria['offset'])) {
            $this->offset = $criteria['offset'];
        }
        if (isset($criteria['limit']) && $criteria['limit'] < 50) {
            $this->limit = $criteria['limit'];
        }
        /** @var Builder $qr */
        $qr = GestionBudget::orderBy('id','DESC');
        foreach ($criteria as $key => $value) {
            if ($value !== null) {
                switch ($key) {
                    case 'user_id':
                        $qr->where('user_id', '=', $value);
                        break;
                }
            }
        }
        return $qr->offset($this->offset)->limit($this->limit)->get()->map->format();
    }

}
