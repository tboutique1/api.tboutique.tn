<?php

namespace App\Http\Controllers\gestionBudget;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GestionBudgetSearchController extends Controller
{
    private $GestionBudgetRepository;

    public function __construct(GestionBudgetRepository $GestionBudgetRepository)
    {
        $this->GestionBudgetRepository = $GestionBudgetRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Illuminate\Http\Request
     */
    public function store(Request $request)
    {
        $gestionBudgets = $this->GestionBudgetRepository->searchWithCriteria($request->all());
        return $gestionBudgets;
    }

    public function getTotal()
    {
        $total = 0;
        $gestionBudgets = $this->GestionBudgetRepository->searchWithCriteria(['user_id' => Auth::user()->id]);
        foreach ($gestionBudgets as $gestionBudget) {
            if ($gestionBudget['action'] == 1) {
                $total +=  $gestionBudget['montant'];
          } else {
                $total -=  $gestionBudget['montant'];
            }
        }
        return $total;
    }

    public function getTotalbyIdUser($id)
    {
        $total = 0;
        $gestionBudgets = $this->GestionBudgetRepository->searchWithCriteria(['user_id' => $id]);
        foreach ($gestionBudgets as $gestionBudget) {
            if ($gestionBudget['action'] == 1) {
                $total +=  $gestionBudget['montant'];
            } else {
                $total -=  $gestionBudget['montant'];
            }
        }
        return $total;
    }
}
