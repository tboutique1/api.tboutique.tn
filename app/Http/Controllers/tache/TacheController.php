<?php

namespace App\Http\Controllers\tache;

use App\Http\Controllers\Controller;
use App\Http\Controllers\historique\HistoriqueController;
use App\Http\Requests\TacheCreateRequest;
use App\Models\Tache;
use App\Models\TacheImages;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class TacheController extends Controller
{
    protected $user;

    public function __construct(HistoriqueController $historiqueController)
    {
        if (JWTAuth::getToken()) {
            $this->user = JWTAuth::parseToken()->authenticate();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tache::where('user_id', '=', Auth::user()->id)->orderBy('order')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $param = $request->all();
            $param['user_id'] = Auth::user()->id;
            $res = Tache::create($param);

            if ($res) {
                return response()->json(['data' => $res->format(), 'message' => 'Tache cree avec succee'], 200);
            } else {
                return response()->json(['error' => 'Echec creation Tache'], 400);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tache $tache)
    {
        return $tache->format();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tache $tache)
    {
        $param = $request->all();
        $res = $tache->update($param);

        if ($res) {
            return response()->json(['data' => $tache->format(), 'message' => 'Tache cree avec succee'], 200);
        } else {
            return response()->json(['error' => 'Echec creation Tache'], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tache $tache)
    {
        $res = $tache->delete();
        if ($res) {
            return response()->json(['message' => 'Tache modifier avec succee'], 200);
        } else {
            return response()->json(['error' => 'Echec supprimer Tache'], 400);
        }

    }
}
