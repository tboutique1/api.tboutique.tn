<?php


namespace App\Http\Controllers\tache;


use App\Models\Tache;
use Illuminate\Database\Query\Builder;

class TacheRepository
{
    public function searchWithCriteria($criteria)
    {
        /** @var Builder $qr */
        $qr = Tache::orderBy('id','DESC');
        foreach ($criteria as $key => $value) {
            if ($value !== null) {
                switch ($key) {
                    case 'titre':
                        $qr->where('titre', 'like', '%' . $value . '%');
                        break;
                    case 'inf_egal_date_at':
                        $qr->where('date', '<=', $value);
                        break;
                    case 'sup_heure_at':
                        $qr->where('heure', '>', $value);
                        break;
                    case 'user_id':
                        $qr->where('user_id', '=', $value);
                        break;
                }
            }
        }
        var_dump($qr->toSql());
        return $qr->get()->map->format();

    }
}
