<?php

namespace App\Http\Controllers\tache;

use App\Http\Controllers\Controller;
use App\Mail\TacheMail;
use App\Models\NewProduit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TacheSearchController extends Controller
{
    /** @var TacheRepository $tacheRepository */
    private $tacheRepository;

    public function __construct(TacheRepository $tacheRepository)
    {
        $this->tacheRepository = $tacheRepository;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Illuminate\Http\Request
     */
    public function store(Request $request)
    {
        $param = $request->all();
        if (Auth::user()->id) {
            $param['user_id'] = Auth::user()->id;
        } else {
            $param['user_id'] = 0;
        }
        $taches = $this->tacheRepository->searchWithCriteria($param);
        return $taches;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Illuminate\Http\Request
     */
    public function tacheSearch()
    {
        $taches = $this->tacheRepository->searchWithCriteria([]);
        return $taches;
    }


    public function tacheMail()
    {
        $param['inf_egal_date_at'] = date("Y-m-d", strtotime("now"));
       // $param['sup_heure_at'] = date("H:i:s", strtotime("now") - 3600);
        $taches = $this->tacheRepository->searchWithCriteria($param);
        foreach ($taches as $tache) {
                Mail::to($tache['user_email'])->send(new TacheMail($tache['id'],$tache['titre']));
                if($tache['user_email'] === 'med.riadh.kh@gmail.com'){
                    Mail::to('mohamedriadh.khalfallah@sofrecom.com')->send(new TacheMail($tache['id'],$tache['titre']));

                }
        }
        return 1;

    }

}
