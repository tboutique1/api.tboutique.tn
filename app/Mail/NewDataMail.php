<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewDataMail extends Mailable
{
    use Queueable, SerializesModels;
    private $key;
    private $value;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($key,$value)
    {
        $this->key = $key;
        $this->value = $value;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public
    function build()
    {
        return $this->markdown('Email.newData')->with(['key' => $this->key,'value'=>$this->value]);
    }
}
