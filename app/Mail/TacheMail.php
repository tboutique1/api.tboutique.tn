<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TacheMail extends Mailable
{
    use Queueable, SerializesModels;
    private $id;
    private $titre;
    private $baseUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id,$titre)
    {
        $this->baseUrl = config('front.FRONT_URL');
        $this->id = $id;
        $this->titre = $titre;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public
    function build()
    {
        return $this->markdown('Email.notification')->with(['titre' => $this->titre,'url'=>$this->baseUrl.'/newProduit/view/'.$this->id]);
    }
}
