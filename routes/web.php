<?php

use App\Http\Controllers\newProduit\NewProduitRepository;
use App\Mail\NotificationMail;
use App\Models\Delegation;
use App\Models\Modele;
use Goutte\Client;
use Illuminate\Support\Facades\Route;
use function App\Http\Controllers\scraping\get_http_response_code;
use Symfony\Component\HttpClient\HttpClient;// or use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Notifications;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',
    function () {
        return view('facebookwelcome');
    });

Route::get('/test', function () {
    $data['url']='http://www.tunisie-annonce.com/AnnoncesImmobilier.asp';
    $data['site']='tunisie-annonce';
    $data['category_id']=1;
    $sc=new \App\Http\Controllers\scraping\Scraping();
    $sc->addAlldataFromTunisieAnnonce($data);
    // $newProduit=\App\Models\NewProduit::first();
   // Notification::send($newProduit,new \App\Notifications\NewsWasPublished());
    try {
        \Illuminate\Support\Facades\DB::connection()->getPdo();
        var_dump("here");
    } catch (\Exception $e) {
        die("Could not connect to the database.  Please check your configuration. error:" . $e);
    }
    return view('welcome');
});
Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});
Route::get('/produit/{newProduit}', [\App\Http\Controllers\newProduit\NewProduitController::class, 'produitView']);
