<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('titre')->nullable();
            $table->integer('prix_min')->nullable();
            $table->integer('prix_max')->nullable();
            $table->unsignedBigInteger('societe_id');
            $table->foreign('societe_id')->references('id')->on('societes');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('sous_category_id')->nullable();
            $table->foreign('sous_category_id')->references('id')->on('sous_categories');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedBigInteger('delegation_id')->nullable();
            $table->foreign('delegation_id')->references('id')->on('delegations');
            $table->unsignedBigInteger('gouvernorat_id')->nullable();
            $table->foreign('gouvernorat_id')->references('id')->on('gouvernorats');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
