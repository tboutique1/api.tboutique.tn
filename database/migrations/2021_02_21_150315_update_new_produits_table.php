<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_produits', function (Blueprint $table) {
            $table->string('reference')->nullable();
            $table->integer('paiement_facilite_3_mois')->nullable();
            $table->integer('paiement_facilite_6_mois')->nullable();
            $table->integer('paiement_facilite_12_mois')->nullable();
            $table->integer('prix_achat');
            $table->integer('prix_sold')->nullable();
            $table->string('url_externe')->nullable();
            $table->string('etat_produit')->nullable();
            $table->boolean('etat');
            $table->unsignedBigInteger('delegation_id');
            $table->foreign('delegation_id')->references('id')->on('delegations');
            $table->string('autre_delegation')->nullable();
            $table->unsignedBigInteger('gouvernorat_id');
            $table->foreign('gouvernorat_id')->references('id')->on('gouvernorats');
            $table->string('autre_gouvernorat')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('adresse');
            $table->string('complement_adresse')->nullable();
            $table->integer('chambres')->nullable();
            $table->integer('puissanceFiscale')->nullable();
            $table->integer('kilometrage')->nullable();
            $table->integer('annee')->nullable();
            $table->integer('superficie')->nullable();
            $table->string('typeTransaction')->nullable();
            $table->string('couleur')->nullable();
            $table->string('typeCarrosserie')->nullable();
            $table->string('boite')->nullable();
            $table->string('cylindre')->nullable();
            $table->string('carburant')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
