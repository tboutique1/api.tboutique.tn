<?php

namespace Database\Seeders;

use App\Models\Marque;
use App\Models\Modele;
use Illuminate\Database\Seeder;

class MarqueVoituresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $resul = Marque::create(['etat' => true, 'nom' => 'Alfa Romeo', 'image_path' => 'alfa-romeo.jpg', 'image_name' => 'alfa-romeo.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"1975":"1975"},{"1976":"1976"},{"1977":"1977"},{"1978":"1978"},{"1979":"1979"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Aston Martin', 'image_path' => '.jpg', 'image_name' => '.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10826":"V8"},{"11278":"VH260"},{"11282":"VH3"},{"11279":"VH309"},{"11280":"VH350"},{"11281":"VH351"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Audi', 'image_path' => 'audi.jpg', 'image_name' => 'audi.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10096":"100"},{"10097":"200"},{"696":"80"},{"10095":"90"},{"870":"A1"},{"979":"A1 SPORTBACK"},{"16":"A2"},{"17":"A3"},{"928":"A3 CABRIO"},{"11305":"A3 SEDAN"},{"929":"A3 SPORTBACK"},{"18":"A4"},{"804":"A4 ALL ROAD"},{"11304":"A4 AVANT"},{"19":"A4 CABRIO"},{"11306":"A4 LIMO"},{"11307":"A4L"},{"611":"A5"},{"805":"A5 CABRIO"},{"980":"A5 Coup\u00e9"},{"806":"A5 SPORTBACK"},{"20":"A6"},{"21":"A6 ALLROAD"},{"11303":"A6 AVANT"},{"11308":"A6 LIMO"},{"11309":"A6L"},{"912":"A7"},{"10109":"A7 SPORTBACK"},{"22":"A8"},{"10101":"A8 L"},{"23":"ALLROAD"},{"24":"ALLROAD QUATTRO"},{"25":"B6"},{"26":"CABRIO"},{"697":"Coup\u00e9"},{"11217":"E-TRON"},{"11218":"E-TRON S"},{"11219":"E-TRON S SPORTBACK"},{"11220":"E-TRON SPORTBACK"},{"10204":"Q2"},{"964":"Q3"},{"11302":"Q3 SPORTBACK"},{"782":"Q5"},{"27":"Q7"},{"11315":"Q7S"},{"10203":"Q8"},{"670":"QUATTRO"},{"28":"R8"},{"11297":"R8 COUPE"},{"11310":"R8 COUPE V10 PLUS"},{"11317":"R8 COUPE V8"},{"11316":"R8 GT SPYDER"},{"11301":"R8 SPYDER"},{"11296":"RS 3"},{"11300":"RS 3 SEDAN"},{"11295":"RS 3 SPORTBACK"},{"11294":"RS 4 AVANT"},{"11319":"RS 5 CABRIO"},{"11293":"RS 6 AVANT"},{"11292":"RS Q3"},{"11291":"RS Q8"},{"698":"RS2"},{"29":"RS4"},{"10103":"RS5 CABRIO"},{"10102":"RS5 COUPE"},{"783":"RS6"},{"10104":"RS7 SPORTBACK"},{"11311":"S1"},{"11312":"S1 SPORTBACK"},{"30":"S3"},{"10105":"S3 CABRIO"},{"11290":"S3 SEDAN"},{"11289":"S3 SPORTBACK"},{"31":"S4"},{"11288":"S4 AVANT"},{"11318":"S4 CABRIO"},{"612":"S5"},{"10106":"S5 CABRIO"},{"10107":"S5 COUPE"},{"10108":"S5 SPORTBACK"},{"32":"S6"},{"11287":"S6 AVANT"},{"10208":"S7"},{"33":"S8"},{"11283":"SQ2"},{"11284":"SQ5"},{"11285":"SQ7"},{"11286":"SQ8"},{"34":"TT"},{"35":"TT Coup\u00e9"},{"11314":"TT QUATTRO SPORT"},{"36":"TT ROADSTER"},{"11299":"TT RS COUPE"},{"11298":"TT RS ROADSTER"},{"11313":"TT S-LINE"},{"10100":"TTS"},{"10098":"TTS COUPE"},{"10099":"TTS ROADSTER"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Blmc-Rover', 'image_path' => 'blmc-rover.jpg', 'image_name' => 'blmc-rover.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"699":"111"},{"640":"200"},{"700":"214"},{"701":"216"},{"37":"25"},{"641":"400"},{"38":"45"},{"642":"600"},{"39":"75"},{"643":"800"},{"40":"CITYROVER"},{"644":"COUPE\'"},{"41":"DEFENDER"},{"42":"DISCOVERY"},{"43":"DISCOVERY 3"},{"10633":"DISCOVERY SPORT"},{"44":"FREELANDER"},{"671":"LAND ROVER"},{"702":"METRO"},{"45":"MG F"},{"703":"MG RV8"},{"46":"MG TF"},{"704":"MG V8"},{"47":"MG ZR"},{"48":"MG ZS"},{"49":"MG ZT\/ZT-T"},{"50":"MG-TF"},{"51":"MG-ZR"},{"52":"MG-ZS"},{"53":"MG-ZT"},{"645":"MINI"},{"672":"R100"},{"673":"R200"},{"674":"R400"},{"705":"R600"},{"706":"R800"},{"54":"RANGE ROVER"},{"55":"RANGE ROVER SPORT"},{"10632":"RANGE ROVER VELAR"},{"56":"STEETWISE"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Bmw', 'image_path' => 'bmw.jpg', 'image_name' => 'bmw.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"11339":"1 SERIE 3 DOOR"},{"11355":"2002"},{"11348":"503"},{"11344":"ACTIVE"},{"11340":"ACTIVE HYBRID 3"},{"11335":"ACTIVE HYBRID 5"},{"11345":"B6 ALPINA"},{"11346":"B7 ALPINA"},{"675":"CABRIO"},{"11351":"E SERIES"},{"1083":"I3"},{"1084":"I8"},{"11321":"IX3"},{"11349":"LSC"},{"11331":"M COUPE"},{"11334":"M ROADSTER"},{"11320":"M1"},{"10114":"M2"},{"11322":"M3"},{"11333":"M3 CABRIO"},{"10117":"M3 COUPE"},{"10111":"M4"},{"10112":"M4 CABRIO"},{"11347":"M4 CONVERTIBLE"},{"10113":"M4 COUPE"},{"10115":"M5"},{"10116":"M6"},{"11323":"M8"},{"57":"SERIE 1"},{"11338":"SERIE 1 CABRIO"},{"11337":"SERIE 1 COUPE"},{"10128":"SERIE 1 SEDAN"},{"10127":"SERIE 1 TOURING"},{"1070":"SERIE 2"},{"10118":"SERIE 2 CABRIO"},{"10119":"SERIE 2 GT"},{"58":"SERIE 3"},{"59":"SERIE 3 CABRIOLET"},{"60":"SERIE 3 COMPACT"},{"61":"SERIE 3 Coup\u00e9"},{"11336":"SERIE 3 GT"},{"10129":"SERIE 3 LIMOUSINE"},{"10120":"SERIE 3 TOURING"},{"1057":"SERIE 4"},{"10121":"SERIE 4 GRAN COUPE"},{"11343":"SERIE 4 GT"},{"62":"SERIE 5"},{"10122":"SERIE 5 GT"},{"10123":"SERIE 5 LIMOUSINE"},{"10124":"SERIE 5 TOURING"},{"63":"SERIE 6"},{"10125":"SERIE 6 CABRIO"},{"10126":"SERIE 6 COUPE"},{"64":"SERIE 7"},{"10130":"SERIE 7 LIMOUSINE"},{"65":"SERIE 8"},{"66":"SERIE M"},{"646":"SERIE M3"},{"11350":"SERIE Z"},{"849":"X1"},{"11332":"X1 SPORTSTOURER"},{"10242":"X2"},{"71":"X3"},{"11324":"X3-M"},{"1085":"X4"},{"11325":"X4-M"},{"72":"X5"},{"11326":"X5-M"},{"742":"X6"},{"11327":"X6-M"},{"11328":"X7"},{"11329":"X7-M"},{"846":"Z3"},{"11354":"Z3 COUPE"},{"847":"Z3 Coup\u00e9 Roadster"},{"11353":"Z3 ROADSTER"},{"73":"Z4"},{"11341":"Z4 COUPE"},{"11330":"Z4 ROADSTER"},{"11342":"Z4-M"},{"848":"Z8"},{"11352":"Z8 ROADSTER"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Chevrolet / Daewoo', 'image_path' => 'chevrolet.jpg', 'image_name' => 'chevrolet.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"11373":"ALPHEON"},{"112":"AVEO"},{"982":"CAMARO"},{"113":"CAPTIVA"},{"11374":"CHEVINIVA"},{"983":"CORVETTE"},{"850":"CRUZE"},{"114":"EPICA"},{"115":"EVANDA"},{"743":"HHR"},{"116":"KALOS"},{"117":"LACETTI"},{"118":"LANOS"},{"119":"LEGANZA"},{"984":"MALIBU"},{"120":"MATIZ"},{"121":"NUBIRA"},{"930":"ORLANDO"},{"122":"REZZO"},{"871":"SPARK"},{"124":"TACUMA"},{"1036":"TRAX"},{"985":"VOLT"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Chrysler / Jeep', 'image_path' => 'chrysler-jeep.jpg', 'image_name' => 'chrysler-jeep.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"11376":"200"},{"94":"300 C"},{"11377":"300 SERIES"},{"96":"300M"},{"11391":"ASPEN"},{"745":"AVENGER"},{"614":"CALIBER"},{"10443":"CARAVAN"},{"11385":"CHALLENGER"},{"11378":"CHARGER"},{"97":"CHEROKEE"},{"99":"COMMANDER"},{"615":"COMPASS"},{"100":"CROSSFIRE"},{"11386":"DAKOTA"},{"11379":"DART"},{"10511":"DODGE"},{"11380":"DURANGO"},{"11375":"GLADIATOR"},{"11381":"GRAND CARAVAN"},{"101":"GRAND CHEROKEE"},{"744":"GRAND VOYAGER"},{"11393":"INTREPID"},{"10614":"JEEP"},{"746":"JOURNEY"},{"11389":"LIBERTY"},{"11388":"MAGNUM"},{"102":"NEON"},{"616":"NITRO"},{"11387":"PACIFICA"},{"747":"PATRIOT"},{"103":"PT CRUISER"},{"104":"PT CRUISER CABRIO"},{"11390":"RAM"},{"11382":"RAM PROMASTER"},{"11383":"RAM PROMASTER CITY"},{"10512":"RAM TRUCK"},{"1071":"RENEGADE"},{"11392":"SEBR\/STRATUS COUPE"},{"105":"SEBRING"},{"106":"SEBRING CABRIO"},{"10674":"SPRINTER"},{"107":"STRATUS"},{"11384":"TOWN & COUNTRY"},{"648":"VAGONEER"},{"108":"VIPER"},{"709":"VISION"},{"109":"VOYAGER"},{"110":"WAGON"},{"111":"WRANGLER"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Citroen', 'image_path' => 'citroen.jpg', 'image_name' => 'citroen.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10041":"2CV"},{"10171":"2CV CHARLESTON"},{"10173":"2CV COCORICO"},{"10867":"2CV DOLLY"},{"10172":"2CV FRANCE 3"},{"10873":"2CV JAMES BOND 007"},{"10888":"2CV SPOT"},{"10868":"ACADIANE"},{"676":"AX"},{"10864":"AXEL"},{"10874":"AY"},{"10871":"AYU"},{"10872":"AZ"},{"74":"BERLINGO"},{"707":"BX"},{"851":"C CROSSER"},{"913":"C ZERO"},{"75":"C1"},{"76":"C15"},{"77":"C2"},{"10862":"C25"},{"78":"C3"},{"10655":"C3 AIRCROSS"},{"807":"C3 PICASSO"},{"79":"C3 PLURIEL"},{"10863":"C35"},{"80":"C4"},{"981":"C4 AIRCROSS"},{"1081":"C4 CACTUS"},{"10897":"C4 CACTUS M"},{"828":"C4 Coup\u00e9"},{"808":"C4 GRAND PICASSO"},{"82":"C4 PICASSO"},{"10902":"C4 SPACETOURER"},{"10859":"C4 TRIOMPHE"},{"10892":"C4L"},{"83":"C5"},{"10656":"C5 AIRCROSS"},{"84":"C6"},{"85":"C8"},{"613":"CROSSER"},{"10821":"CX"},{"10042":"DS"},{"874":"DS3"},{"1037":"DS3 CC"},{"10903":"DS3 CROSSBACK"},{"10895":"DS3 MODERN"},{"1038":"DS3 RACING"},{"931":"DS4"},{"10900":"DS4 CROSSBACK"},{"966":"DS5"},{"10901":"DS7 CROSSBACK"},{"10907":"DS9"},{"10905":"E-BERLINGO MULTISPACE"},{"10908":"E-C4"},{"10906":"E-MEHARI"},{"10860":"ELYSEE"},{"86":"EVASION"},{"10861":"FUKANG"},{"10904":"GRAND C4 SPACETOURER"},{"10870":"GSA"},{"10880":"HY"},{"87":"JUMPER"},{"88":"JUMPY"},{"10869":"LNA"},{"10866":"MEHARI"},{"748":"NEMO"},{"89":"SAXO"},{"10899":"SPACETOURER"},{"10865":"VISA"},{"90":"XANTIA"},{"91":"XM"},{"92":"XSARA"},{"93":"XSARA PICASSO"},{"677":"ZX"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Dacia', 'image_path' => 'dacia.jpg', 'image_name' => 'dacia.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"1030":"DOKKER"},{"900":"DUSTER"},{"1000":"LODGY"},{"832":"LOGAN"},{"833":"LOGAN MCV"},{"834":"LOGAN PICK UP"},{"835":"LOGAN VAN"},{"836":"SANDERO"},{"10811":"SANDERO STEPWAY"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Daihatsu', 'image_path' => 'daihatsu.jpg', 'image_name' => 'daihatsu.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"11176":"BOON"},{"11178":"CAST"},{"965":"CHARADE"},{"125":"COPEN"},{"126":"CUORE"},{"10243":"DOMINO"},{"127":"FEROZA"},{"128":"GRAN MOVE"},{"617":"MATERIA"},{"129":"MOVE"},{"11179":"MOVE CUSTOM"},{"130":"SIRION"},{"11177":"TANTO"},{"11180":"TANTO CUSTOM"},{"131":"TERIOS"},{"132":"TREVIS"},{"133":"YRV"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Ferrari / Maserati', 'image_path' => 'ferrari-maserati.jpg', 'image_name' => 'ferrari-maserati.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10621":"360 MODENA"},{"10514":"599 GTB Fiorano "},{"10622":"612 SCAGLIETTI"},{"10623":"CALIFORNIA"},{"10624":"F40"},{"10620":"F430"},{"10513":"FF"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Fiat', 'image_path' => 'fiat.jpg', 'image_name' => 'fiat.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10078":"124 SPIDER"},{"10083":"126"},{"618":"500"},{"878":"500 ABARTH"},{"1040":"500 ABARTH OPENING EDITION"},{"1041":"500 ABARTH ZEROCENTO"},{"10630":"500 ACTION"},{"852":"500 C"},{"932":"500 C ABARTH"},{"1042":"500 C ABARTH ITALIA"},{"10079":"500 GIARDINIERA"},{"10629":"500 ICON"},{"1015":"500 L"},{"1060":"500 L LIVING"},{"1061":"500 L TREKKING"},{"10628":"500 LA PRIMA"},{"10080":"500 TOPOLINO"},{"10077":"500 X"},{"1016":"595 ABARTH COMPETIZIONE"},{"1017":"595 ABARTH TURISMO"},{"649":"600"},{"1020":"695 ABARTH EDIZIONE MASERATI"},{"933":"695 ABARTH TRIBUTO FERRARI"},{"135":"BARCHETTA"},{"136":"BRAVA"},{"137":"BRAVO"},{"138":"COUPE\'"},{"139":"CROMA"},{"140":"DOBLO\'"},{"141":"DOBLO\' CARGO"},{"142":"DUCATO"},{"10081":"DUNA"},{"143":"FIORINO"},{"934":"FREEMONT"},{"10649":"FULLBACK"},{"144":"GRANDE PUNTO"},{"879":"GRANDE PUNTO ABARTH"},{"145":"IDEA"},{"809":"LINEA"},{"650":"MAREA"},{"147":"MARENGO"},{"148":"MULTIPLA"},{"651":"PALIO"},{"151":"PALIO 2V"},{"152":"PALIO SW"},{"153":"PALIO WEEK END"},{"154":"PANDA"},{"1072":"PANDA 4X4"},{"801":"PANDA ALESSI"},{"155":"PANDA CROSS"},{"802":"PANDA MAMY"},{"156":"PANDA VAN"},{"157":"PUNTO"},{"1043":"PUNTO ABARTH"},{"710":"PUNTO CABRIO"},{"877":"PUNTO EVO"},{"158":"PUNTO VAN"},{"880":"QUBO"},{"10082":"REGATA"},{"159":"SCUDO"},{"160":"SEDICI"},{"161":"SEICENTO"},{"162":"SEICENTO BRUSH"},{"711":"SPIDER"},{"163":"STILO"},{"164":"STILO SW"},{"165":"STRADA"},{"712":"TEMPRA"},{"713":"TIPO"},{"167":"ULYSSE"},{"714":"UNO"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Ford', 'image_path' => 'ford.jpg', 'image_name' => 'ford.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"1021":"B-MAX"},{"935":"C-MAX"},{"652":"COUGAR"},{"1073":"ECOSPORT"},{"168":"ESCORT"},{"678":"ESCORT CABRIO"},{"169":"FIESTA"},{"1074":"FIESTA ST"},{"170":"FOCUS"},{"171":"FOCUS C-MAX"},{"619":"FOCUS CABRIO"},{"810":"FOCUS CC"},{"811":"FOCUS RS"},{"881":"FOCUS ST"},{"172":"FUSION"},{"173":"GALAXY"},{"174":"KA"},{"749":"KUGA"},{"679":"MAVERICK"},{"175":"MONDEO"},{"176":"PUMA"},{"177":"RANGER"},{"178":"S-MAX"},{"680":"SCORPIO"},{"179":"STREET KA"},{"180":"TOURNEO"},{"181":"TRANSIT"},{"10198":"EXPLORER"},{"10641":"F-SERIES"},{"10640":"MERCURY"},{"10094":"MUSTANG"},{"11191":"MUSTANG MACH-E"},{"10637":"PROBE"},{"10642":"RANGER"},{"10199":"SPORT TRAC EXPLORER"},{"10625":"THUNDERBIRD"},{"10639":"THUNDERBIRD"},{"10638":"TRACER"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'General Motors', 'image_path' => '.jpg', 'image_name' => '.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10651":"BUICK"},{"10652":"CADILLAC"},{"10669":"CORVETTE"},{"10648":"ESCALADE"},{"10647":"GTO"},{"10812":"METRO"},{"10650":"PONTIAC"},{"10813":"TRACKER"},{"10814":"TRANS SPORT"},{"10670":"XLR"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Honda', 'image_path' => 'honda.jpg', 'image_name' => 'honda.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"183":"ACCORD"},{"184":"CIVIC"},{"185":"CIVIC Coup\u00e9"},{"620":"CIVIC HYBRID"},{"792":"CM-V"},{"186":"CR-V"},{"882":"CR-Z"},{"10474":"CRX"},{"187":"FR-V"},{"188":"HR-V"},{"189":"INSIGHT"},{"190":"INTEGRA"},{"191":"INTEGRA TYPE R"},{"192":"JAZZ"},{"193":"LEGEND"},{"194":"LOGO"},{"195":"NSX"},{"10244":"ODYSSEY"},{"196":"PRELUDE"},{"197":"S2000"},{"10245":"SABER"},{"653":"SHUTTLE"},{"198":"STREAM"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Hyundai', 'image_path' => 'hyundai.jpg', 'image_name' => 'hyundai.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"199":"ACCENT"},{"200":"ATOS"},{"201":"ATOS PRIME"},{"202":"CENTENNIAL"},{"203":"CONTINENTAL"},{"204":"COUPE\'"},{"205":"ELANTRA"},{"206":"GALLOPER"},{"207":"GALLOPER II"},{"1086":"GENESIS"},{"938":"GENESIS COUPE\'"},{"208":"GETZ"},{"209":"GRANDEUR"},{"210":"H-1"},{"211":"H-1 TRK"},{"212":"H-1 VAN"},{"214":"H100"},{"215":"H100 TRK"},{"750":"I10"},{"812":"I20"},{"621":"I30"},{"967":"I40"},{"10658":"IONIQ"},{"915":"IX20"},{"883":"IX35"},{"813":"IX55"},{"10657":"KONA"},{"681":"LANTRA"},{"216":"MATRIX"},{"654":"MX"},{"217":"NEW ACCENT"},{"715":"PONY"},{"655":"RD"},{"218":"SANTA FE"},{"10594":"SANTAMO"},{"219":"SONATA"},{"220":"SONICA"},{"221":"TERRACAN"},{"222":"TRAJET"},{"223":"TUCSON"},{"968":"VELOSTER"},{"1014":"VELOSTER TURBO"},{"622":"VERACRUZ"},{"656":"X-3"},{"224":"XG"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Isuzu', 'image_path' => 'isuzu.jpg', 'image_name' => 'isuzu.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"225":"BIG HORN"},{"226":"D-MAX"},{"227":"PICK-UP"},{"228":"RODEO"},{"229":"TROOPER"},{"230":"TRUCK"},{"231":"WIZARD"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Jaguar', 'image_path' => 'jaguar.jpg', 'image_name' => 'jaguar.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"716":"DAIMLER"},{"10440":"E PACE"},{"10438":"F PACE"},{"1039":"F TYPE"},{"10439":"I PACE"},{"1022":"R"},{"232":"S TYPE"},{"234":"X TYPE"},{"1087":"XE"},{"751":"XF"},{"717":"XJ"},{"718":"XJ R"},{"719":"XJ SPORT"},{"235":"XJ TYPE"},{"752":"XK"},{"236":"XK TYPE"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Kia', 'image_path' => 'kia.jpg', 'image_name' => 'kia.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"816":"AMANTI"},{"237":"CARENS"},{"238":"CARNIVAL"},{"239":"CEE\'D"},{"986":"CEE\'D SW"},{"240":"CERATO"},{"241":"JOICE"},{"242":"K 2500"},{"10665":"K3"},{"243":"MAGENTIS"},{"244":"OPIRUS"},{"245":"OPTIMA"},{"246":"PICANTO"},{"247":"PREGIO"},{"248":"PRIDE"},{"784":"PRO CEE\'D"},{"249":"RIO"},{"10666":"SELTOS"},{"250":"SEPHIA"},{"253":"SHUMA"},{"255":"SORENTO"},{"817":"SOUL"},{"256":"SPORTAGE"},{"884":"VENGA"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Lada', 'image_path' => 'lada.jpg', 'image_name' => 'lada.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10663":"GRAND"},{"1058":"GRANTA"},{"814":"KALINA"},{"10660":"LARGUS"},{"258":"NIVA"},{"815":"PRIORA"},{"10662":"VESTA"},{"10661":"XRAY"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Lamborghini', 'image_path' => 'lamborghini.jpg', 'image_name' => 'lamborghini.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"939":"AVENTADOR"},{"940":"ESTOQUE"},{"885":"GALLARDO"},{"10659":"HURACAN"},{"886":"MURCIELAGO"},{"887":"REVENTON"},{"682":"ROADSTEAR"},{"10207":"URUS"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Lancia', 'image_path' => 'lancia.jpg', 'image_name' => 'lancia.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"657":"DEDRA"},{"259":"DELTA"},{"989":"FLAVIA"},{"10611":"FULVIA"},{"260":"K"},{"261":"K COUPE\'"},{"262":"K SW"},{"263":"LYBRA"},{"264":"MUSA"},{"265":"PHEDRA"},{"969":"THEMA"},{"266":"THESIS"},{"970":"VOYAGER"},{"267":"Y"},{"720":"Y 10"},{"268":"Y ELEFANTINO BLU"},{"269":"Y ELEFANTINO ROSSO"},{"270":"YPSILON"},{"753":"YPSILON MOMO"},{"271":"Z"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Land Rover', 'image_path' => 'land-rover.jpg', 'image_name' => 'land-rover.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"754":"DEFENDER"},{"755":"DISCOVERY"},{"1088":"DISCOVERY SPORT"},{"756":"FREELANDER"},{"942":"FREELANDER 2"},{"757":"RANGE ROVER"},{"943":"RANGE ROVER EVOQUE"},{"758":"RANGE ROVER SPORT"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Lexus', 'image_path' => '.jpg', 'image_name' => '.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10251":"CT200h"},{"10252":"ES300h"},{"10260":"GS300h"},{"10261":"GS450h"},{"10255":"IS300h"},{"10257":"LC500h"},{"10253":"NX300h"},{"10258":"RC300h"},{"10254":"RX450h"},{"10256":"UX250h"},{"10259":"ux300e"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Lotus', 'image_path' => 'lotus.jpg', 'image_name' => 'lotus.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10827":"ELISE"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Mazda', 'image_path' => 'mazda.jpg', 'image_name' => 'mazda.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"278":"121"},{"279":"323"},{"280":"323F"},{"281":"626"},{"282":"B-SERIE"},{"623":"BT-50"},{"991":"CX-5"},{"624":"CX-7"},{"625":"CX-9"},{"283":"DEMIO"},{"284":"MAZDA 2"},{"285":"MAZDA 3"},{"286":"MAZDA 5"},{"287":"MAZDA 6"},{"288":"MPV"},{"683":"MX-3"},{"10262":"MX-30"},{"289":"MX-5"},{"290":"PICK-UP"},{"291":"PICK-UP\/B-2500"},{"292":"PREMACY"},{"293":"RX-8"},{"294":"TRIBUTE"},{"295":"XEDOS"},{"296":"XEDOS 9"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Mercedes', 'image_path' => 'mercedes.jpg', 'image_name' => 'mercedes.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10634":"AMG GT BLACK SERIES"},{"1031":"CITAN"},{"297":"CL"},{"1046":"CLA"},{"299":"CLASSE A"},{"300":"CLASSE B"},{"301":"CLASSE C"},{"302":"CLASSE E"},{"303":"CLASSE G"},{"304":"CLASSE GL"},{"305":"CLASSE M"},{"306":"CLASSE R"},{"307":"CLASSE S"},{"308":"CLASSE V"},{"762":"CLC"},{"309":"CLK"},{"310":"CLK Coup\u00e9"},{"311":"CLS"},{"659":"COUPE\'"},{"1076":"GLA"},{"10170":"GLE"},{"763":"GLK"},{"10653":"S 65 AMG"},{"312":"SL"},{"313":"SLK"},{"892":"SLS AMG"},{"314":"SPRINTER"},{"315":"VANEO"},{"316":"VARIO"},{"317":"VIANO"},{"318":"VITO"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Mini', 'image_path' => 'mini.jpg', 'image_name' => 'mini.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"319":"MINI"},{"320":"MINI COOPER"},{"321":"MINI COOPER CABRIO"},{"322":"MINI COOPER CHECKMATE"},{"760":"MINI COOPER CLUBMAN"},{"1023":"MINI COOPER CLUBVAN"},{"944":"MINI COOPER COUNTRYMAN"},{"971":"MINI COOPER COUPE\'"},{"1024":"MINI COOPER PACEMAN"},{"323":"MINI COOPER PARK LANE"},{"1077":"MINI COOPER PROVA"},{"992":"MINI COOPER ROADSTER"},{"626":"MINI COOPER S"},{"627":"MINI COOPER S CABRIO"},{"761":"MINI COOPER S CLUBMAN"},{"945":"MINI COOPER S COUNTRYMAN"},{"1025":"MINI COOPER S COUPE\'"},{"1026":"MINI COOPER S PACEMAN"},{"324":"MINI COOPER SEVEN"},{"916":"MINI COUNTRYMAN"},{"325":"MINI ONE"},{"326":"MINI ONE CABRIO"},{"891":"MINI ONE CLUBMAN"},{"1028":"MINI ONE CLUBVAN"},{"946":"MINI ONE COUNTRYMAN"},{"327":"MINI ONE PARK LANE"},{"328":"MINI ONE SEVEN"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Mitsubishi', 'image_path' => 'mitsubishi.jpg', 'image_name' => 'mitsubishi.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"721":"3000GT"},{"893":"ASX"},{"329":"CARISMA"},{"330":"CARISMA EVOLUTION"},{"10580":"CHALLENGER"},{"331":"COLT"},{"332":"COLT CABRIO"},{"722":"ECLIPSE"},{"10591":"FUSO"},{"333":"GALANT"},{"334":"GRANDIS"},{"950":"I-MIEV"},{"335":"L200"},{"336":"L300"},{"337":"L400"},{"338":"LANCER"},{"820":"LANCER EVOLUTION"},{"339":"LANCER EVOLUTION IX"},{"340":"LANCER EVOLUTION VIII"},{"855":"LANCER RALLIART"},{"821":"LANCER SPORTBACK"},{"10582":"MINICA"},{"10581":"MIRAGE"},{"10583":"MONTERO"},{"10589":"MONTERO SPORT"},{"10588":"NATIVA"},{"341":"OUTLANDER"},{"342":"PAJERO"},{"343":"PAJERO PININ"},{"344":"PAJERO SPORT"},{"10585":"SAPPORO"},{"10592":"SIGMA"},{"347":"SPACE GEAR"},{"348":"SPACE RUNNER"},{"349":"SPACE STAR"},{"350":"SPACE WAGON"},{"10584":"STARION"},{"10587":"STATION WAGON"},{"10590":"STRADA"},{"10593":"TRITON"},{"10586":"VALLEY"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Nissan', 'image_path' => 'nissan.jpg', 'image_name' => 'nissan.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"351":"200SX"},{"10645":"240SX"},{"10477":"300ZX"},{"352":"350 Z"},{"353":"350 Z ROADSTER"},{"10137":"370 Z ROADSTER"},{"894":"370Z"},{"354":"ALMERA"},{"355":"ALMERA TINO"},{"356":"ATLEON"},{"357":"CABSTAR"},{"895":"CUBE"},{"973":"EVALIA"},{"904":"EX"},{"905":"FX"},{"906":"G BERLINA"},{"907":"G CABRIO"},{"908":"G COUPE\'"},{"764":"GT-R"},{"358":"INTERSTAR"},{"896":"JUKE"},{"10560":"KING CAB"},{"359":"KUBISTAR"},{"974":"LEAF"},{"917":"M"},{"360":"MAXIMA"},{"361":"MAXIMA QX"},{"362":"MICRA"},{"363":"MICRA C+C"},{"364":"MURANO"},{"951":"NAVARA"},{"366":"NOTE"},{"788":"NP 300 PICK UP"},{"10644":"NP300 NAVARA"},{"897":"NV 200"},{"10673":"NV 250"},{"10672":"NV 300"},{"993":"NV 400"},{"367":"PATHFINDER"},{"368":"PATROL"},{"370":"PICK UP"},{"822":"PIXO"},{"371":"PRIMASTAR"},{"372":"PRIMERA"},{"1089":"PULSAR"},{"1062":"Q50"},{"1063":"Q60"},{"1064":"Q70"},{"952":"QASHQAI"},{"1065":"QX50"},{"1066":"QX70"},{"374":"SERENA"},{"10476":"SKYLINE"},{"724":"TERRANO"},{"378":"TERRANO II"},{"765":"TIIDA"},{"379":"TINO"},{"380":"VANETTE"},{"381":"X-TRAIL"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Opel', 'image_path' => 'opel.jpg', 'image_name' => 'opel.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"1047":"ADAM"},{"382":"AGILA"},{"997":"AMPERA"},{"383":"ANTARA"},{"10087":"ASCONA"},{"384":"ASTRA"},{"725":"ASTRA CABRIO"},{"385":"ASTRA CABRIO COUPE"},{"386":"ASTRA CLASSIC"},{"793":"ASTRA Coup\u00e9"},{"10091":"ASTRA OPC"},{"628":"ASTRA TWIN TOP"},{"387":"ASTRA VAN"},{"388":"ASTRA-ASTRA Coup\u00e9"},{"726":"CALIBRA"},{"10089":"CAMPO"},{"10599":"CARLTON"},{"1053":"CASCADA"},{"10598":"CAVALIER"},{"389":"COMBO"},{"390":"CORSA"},{"10092":"CORSA OPC"},{"660":"CORSA SW"},{"391":"CORSA VAN"},{"10202":"CROSSLAND X"},{"392":"FRONTERA"},{"10246":"GRANDLAND X"},{"629":"GT"},{"823":"INSIGNA"},{"10093":"INSIGNIA OPC"},{"10084":"KADETT"},{"10085":"KADETT CABRIO"},{"10206":"KARL"},{"10086":"MANTA"},{"393":"MERIVA"},{"10597":"MIDI"},{"1032":"MOKKA"},{"10090":"MONTEREY"},{"394":"MOVANO"},{"10595":"NOVA"},{"10596":"NOVAVAN"},{"395":"OMEGA"},{"10088":"SENATOR"},{"396":"SIGNIUM"},{"397":"SINTRA"},{"398":"SPEEDSTER"},{"399":"TIGRA"},{"630":"TIGRA TWIN TOP"},{"10600":"TOUR"},{"400":"VECTRA"},{"401":"VIVARO"},{"11214":"VX220"},{"402":"ZAFIRA"},{"1067":"ZAFIRA TOURER"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Peugeot', 'image_path' => 'peugeot.jpg', 'image_name' => 'peugeot.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"403":"1007"},{"404":"106"},{"10613":"106 ELECTRIC"},{"405":"107"},{"10817":"107 SPORTIUM "},{"10819":"107 URBAN MOVE"},{"10475":"108"},{"1048":"2008"},{"684":"205"},{"10002":"205 GTI"},{"406":"206"},{"408":"206 CABRIO"},{"409":"206 CC"},{"410":"206 SW"},{"10818":"206+"},{"411":"207"},{"412":"207 CC"},{"766":"207 GTI"},{"413":"207 SW"},{"994":"208"},{"824":"3008"},{"414":"306"},{"415":"306 BREAK"},{"416":"306 CABRIO"},{"417":"307"},{"419":"307 CC"},{"420":"307 SW"},{"767":"308"},{"825":"308 CC"},{"826":"308 SW"},{"631":"4007"},{"995":"4008"},{"727":"405"},{"421":"406"},{"423":"406 Coup\u00e9"},{"424":"407"},{"426":"407 Coup\u00e9"},{"794":"407 SW"},{"10174":"408"},{"856":"5008"},{"10626":"504"},{"918":"508"},{"10820":"508 RXH"},{"953":"508 SW"},{"661":"605"},{"427":"607"},{"428":"806"},{"429":"807"},{"768":"BIPPER"},{"827":"BIPPER TEPEE"},{"430":"BOXER"},{"431":"EXPERT"},{"632":"EXPERT TEPEE"},{"996":"ION"},{"10664":"LANDTREK"},{"432":"PARTNER"},{"433":"PARTNER RANCH"},{"789":"PARTNER TEPEE"},{"10263":"PSE"},{"434":"RANCH"},{"898":"RCZ"},{"10175":"TRAVELLER"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Porsche', 'image_path' => 'porsche.jpg', 'image_name' => 'porsche.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10552":"356"},{"435":"911"},{"10047":"911 COUPE"},{"10049":"911 GT3 RS"},{"10048":"911 GTS"},{"10043":"912"},{"1078":"918"},{"10052":"918 SPYDER"},{"10044":"924"},{"662":"928"},{"10045":"930"},{"10046":"944"},{"663":"968"},{"10053":"9X1"},{"436":"BOXSTER"},{"10054":"BOXSTER SPYDER"},{"10051":"CARRERA"},{"437":"CAYENNE"},{"438":"CAYMAN"},{"10050":"GT3 RS"},{"1079":"MACAN"},{"829":"PANAMERA"},{"10616":"TAYCAN"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Renault', 'image_path' => 'renault.jpg', 'image_name' => 'renault.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10822":"ALASKAN"},{"728":"ALPINE"},{"10247":"ARKANA"},{"440":"AVANTIME"},{"10921":"CABRIOLET 19"},{"1049":"CAPTUR"},{"441":"CLIO"},{"857":"CLIO CAMPUS"},{"958":"CLIO RS"},{"10910":"CLIO SPORTER"},{"10071":"CLIO SYMBOLE"},{"10915":"DUSTER"},{"443":"ESPACE"},{"444":"EXPRESS"},{"899":"FLUENCE"},{"10912":"FLUENCE ZE"},{"10070":"FOURGONNETTE"},{"10067":"FUEGO"},{"445":"GRAND ESPACE"},{"790":"GRAND MODUS"},{"830":"GRAND SCENIC"},{"10667":"KADJAR"},{"446":"KANGOO"},{"10919":"KANGOO 4X4"},{"831":"KANGOO BE BOP"},{"795":"KANGOO EXPRESS"},{"769":"KOLEOS"},{"448":"LAGUNA"},{"791":"LAGUNA COUPE\'"},{"10076":"LAGUNA ESTATE"},{"449":"LAGUNA GRANTOUR"},{"959":"LATITUDE"},{"10920":"LE CAR"},{"10918":"LOGAN"},{"10916":"LUTECIA"},{"909":"MASCOTT"},{"450":"MASTER"},{"10069":"MEDALLION"},{"451":"MEGANE"},{"452":"MEGANE CABRIO"},{"453":"MEGANE CABRIO Coup\u00e9"},{"454":"MEGANE Coup\u00e9"},{"10914":"MEGANE ESTATE"},{"455":"MEGANE GRANDTOUR"},{"10824":"MEGANE GT"},{"10072":"MEGANE R26R"},{"960":"MEGANE RS"},{"910":"MEGANE SPORT"},{"633":"MODUS"},{"456":"NEVADA"},{"457":"NUOVA LAGUNA"},{"10250":"ONDINE"},{"10917":"PULSE"},{"10062":"R11"},{"10063":"R12"},{"10909":"R16"},{"10064":"R17"},{"10065":"R18"},{"729":"R19"},{"730":"R21"},{"10923":"R21\/NEVADA"},{"10066":"R25"},{"10060":"R4"},{"731":"R5"},{"10061":"R9"},{"10922":"RAPID"},{"10068":"RODEO"},{"458":"SAFRANE"},{"459":"SCENIC"},{"460":"SCENIC 4X4"},{"911":"SPIDER"},{"10131":"TALISMAN"},{"10913":"THALIA"},{"461":"TRAFIC"},{"464":"TWINGO"},{"10075":"TWINGO GORDINI"},{"10074":"TWINGO GT"},{"10073":"TWINGO RS"},{"998":"TWIZY"},{"465":"VELSATIS"},{"919":"WIND"},{"1050":"ZOE"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Saab', 'image_path' => 'saab.jpg', 'image_name' => 'saab.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"466":"9-3 BERLINA"},{"468":"9-3 CABRIO"},{"796":"9-3 SW"},{"837":"9-3 X"},{"999":"9-4 X"},{"469":"9-5 BERLINA"},{"797":"9-5 SW"},{"685":"900"},{"686":"9000"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Seat', 'image_path' => 'seat.jpg', 'image_name' => 'seat.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"473":"ALHAMBRA"},{"474":"ALTEA"},{"770":"ALTEA FREETRACK"},{"634":"ALTEA XL"},{"475":"AROSA"},{"10619":"ATECA CUPRA"},{"476":"CORDOBA"},{"11188":"CUPRA"},{"10668":"CUPRA BORN"},{"10825":"CUPRA FORMENTOR"},{"838":"EXEO"},{"477":"IBIZA"},{"478":"INCA"},{"479":"LEON"},{"1051":"LEON COPA"},{"10618":"LEON CUPRA R"},{"1052":"LEON SC"},{"1080":"LEON ST"},{"687":"MARBELLA"},{"1001":"MII"},{"10617":"TARRACO"},{"480":"TOLEDO"},{"481":"VARIO"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Simca Talbot', 'image_path' => '.jpg', 'image_name' => '.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10606":"PLEIN CIEL"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Skoda', 'image_path' => 'skoda.jpg', 'image_name' => 'skoda.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"1002":"CITIGO"},{"482":"FABIA"},{"483":"FELICIA"},{"484":"OCTAVIA"},{"485":"PICK UP"},{"976":"PRAKTIK"},{"1054":"RAPID"},{"486":"ROOMSTER"},{"487":"SUPERB"},{"839":"YETI"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Smart', 'image_path' => 'smart.jpg', 'image_name' => 'smart.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"488":"CROSSBLADE"},{"489":"FORFOUR"},{"490":"FORTWO"},{"492":"ROADSTER"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Ssangyong', 'image_path' => 'ssangyong.jpg', 'image_name' => 'ssangyong.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"636":"ACTYON"},{"1029":"ACTYON SPORT"},{"798":"CHAIRMAN"},{"494":"KORANDO"},{"495":"KYRON"},{"496":"MUSSO"},{"498":"REXTON"},{"799":"RODIUS"},{"800":"STAVIC"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Subaru', 'image_path' => 'subaru.jpg', 'image_name' => 'subaru.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"501":"B9 TRIBECA"},{"1003":"BRZ"},{"11182":"CHIFFON"},{"11183":"CHIFFON CUSTOM"},{"502":"FORESTER"},{"503":"IMPREZA"},{"504":"JUSTY"},{"505":"LEGACY"},{"506":"OUTBACK"},{"11181":"STELLA"},{"11184":"STELLA CUSTOM"},{"507":"SUPERB"},{"957":"TREZIA"},{"920":"WRX"},{"1004":"XV"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Suzuki', 'image_path' => 'suzuki.jpg', 'image_name' => 'suzuki.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"508":"ALTO"},{"509":"BALENO"},{"510":"BALENO SW"},{"511":"CARRY"},{"1090":"CELERIO"},{"512":"GRAND VITARA"},{"514":"GRAND VITARA CABRIOLET"},{"516":"IGNIS"},{"517":"JIMMY"},{"518":"JIMMY CABRIO"},{"519":"JIMNY"},{"901":"KIZASHI"},{"520":"LIANA"},{"521":"SAMURAI"},{"688":"SANTANA"},{"771":"SPLASH"},{"522":"SWIFT"},{"1055":"SWIFT 4X4"},{"523":"SX4"},{"1056":"SX4 S-CROSS"},{"524":"VITARA"},{"525":"WAGON R"},{"526":"WAGON R+"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Tesla', 'image_path' => 'tesla.jpg', 'image_name' => 'tesla.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"10139":"Model 3"},{"10141":"Model S"},{"10140":"Model X"},{"10142":"Roadster"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Toyota', 'image_path' => 'toyota.jpg', 'image_name' => 'toyota.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"531":"4 RUNNER"},{"638":"AURIS"},{"532":"AVENSIS"},{"533":"AVENSIS VERSO"},{"534":"AYGO"},{"10201":"C-HR"},{"535":"CAMRY"},{"732":"CARINA"},{"536":"CELICA"},{"537":"COROLLA"},{"11190":"COROLLA CROSS"},{"538":"COROLLA VERSO"},{"539":"CORONA"},{"975":"CT"},{"540":"DYNA"},{"541":"GS"},{"543":"GS300"},{"1007":"GS400"},{"544":"GS430"},{"1033":"GT86"},{"545":"HD TRUCK"},{"546":"HIACE"},{"547":"HILUX"},{"840":"IQ"},{"548":"IS"},{"549":"IS200"},{"550":"IS300"},{"551":"LAND CRUISER"},{"552":"LAND CRUISER 100"},{"553":"LAND CRUISER 90"},{"554":"LAND CRUISER AMAZON"},{"773":"LAND CRUISER V8"},{"555":"LITEACE"},{"556":"LS"},{"1008":"LS400"},{"558":"LS430"},{"10815":"MIRAI"},{"559":"MR2"},{"11185":"PASSO"},{"560":"PICNIC"},{"11186":"PIXIS"},{"11187":"PIXIS JOY"},{"561":"PREVIA"},{"562":"PRIUS"},{"1034":"PRIUS+"},{"1068":"PROACE"},{"563":"RAV 4"},{"564":"RX"},{"565":"RX300"},{"566":"SC"},{"1009":"SC300"},{"1010":"SC400"},{"568":"SC430"},{"10200":"SOLARA"},{"10138":"STARLET"},{"10646":"SUPRA"},{"841":"URBAN CRUISER"},{"961":"VERSO"},{"962":"VERSO S"},{"569":"YARIS"},{"11189":"YARIS CROSS"},{"570":"YARIS VERSO"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Volkswagen', 'image_path' => 'volkswagen.jpg', 'image_name' => 'volkswagen.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"11241":"AERTON"},{"963":"AMAROK"},{"10132":"ARTEON"},{"588":"BEETLE"},{"589":"BEETLE CABRIO"},{"842":"BENTLEY"},{"590":"BORA"},{"591":"CADDY"},{"592":"CALIFORNIA"},{"665":"CARAVELLE"},{"1011":"CC"},{"11260":"CITY GOLF"},{"11261":"CITY JETTA"},{"11266":"COLORADO"},{"738":"CORRADO"},{"593":"CRAFTER"},{"11250":"CROSSGOLF"},{"11251":"CROSSPOLO"},{"11252":"CROSSTOURAN"},{"11253":"CROSSUP"},{"11215":"E-CRAFTER"},{"11216":"E-GOLF"},{"11221":"E-UP"},{"594":"EOS"},{"11265":"EUROVAN"},{"774":"FOX"},{"11263":"GOL"},{"595":"GOLF"},{"11234":"GOLF ALLTRACK"},{"11232":"GOLF BLUE MOTION"},{"596":"GOLF CABRIO"},{"11242":"GOLF GT SPORT"},{"11233":"GOLF GTD"},{"11222":"GOLF GTE"},{"11223":"GOLF GTI"},{"11224":"GOLF GTI TCR"},{"11254":"GOLF PLUS"},{"11240":"GOLF R"},{"11243":"GOLF R32"},{"1082":"GOLF SPORTSVAN"},{"11258":"GOLF TDI"},{"11239":"GOLF TSI"},{"925":"GOLF VARIANT"},{"10248":"ID 3"},{"10249":"ID 4"},{"597":"JETTA"},{"11255":"JETTA GLI"},{"11256":"JETTA SPORTWAGEN"},{"10133":"KAEFER"},{"11257":"LAVIDA"},{"598":"LT"},{"11267":"LT 2"},{"599":"LUPO"},{"600":"MULTIVAN"},{"11244":"MULTIVAN BEACH"},{"601":"PASSAT"},{"1012":"PASSAT ALLTRACK"},{"11238":"PASSAT BLUE MOTION"},{"843":"PASSAT CC"},{"11226":"PASSAT GTE"},{"11237":"PASSAT LIMO"},{"11236":"PASSAT R36"},{"602":"PASSAT VARIANT"},{"11225":"PASSAT VARIANT GTE"},{"11245":"PASSAT XC"},{"603":"PHAETON"},{"11262":"POINTER"},{"604":"POLO"},{"11246":"POLO BLUE GT"},{"11235":"POLO BLUE MOTION"},{"11247":"POLO FUN"},{"11227":"POLO GTI"},{"11228":"POLO TGI"},{"605":"POLO VARIANT"},{"11259":"ROUTAN"},{"11264":"SAVEIRO"},{"779":"SCIROCCO"},{"606":"SHARAN"},{"11229":"T-CROSS"},{"11230":"T-ROC"},{"11231":"T-ROC R"},{"10134":"T4"},{"10135":"T5"},{"10136":"T6"},{"10205":"TARO"},{"777":"TIGUAN"},{"607":"TOUAREG"},{"11248":"TOUAREG FL"},{"11249":"TOUAREG R50"},{"608":"TOURAN"},{"609":"TRANSPORTER"},{"977":"UP"},{"692":"VENTO"}]';
        $this->createModele($mod, $id);

        $resul = Marque::create(['etat' => true, 'nom' => 'Volvo', 'image_path' => 'volvo.jpg', 'image_name' => 'volvo.jpg', 'categorie_id' => 2, 'sous_categorie_id' => 9]);
        $id = $resul->id;
        $mod = '[{"733":"440"},{"734":"460"},{"735":"480"},{"736":"850"},{"737":"900"},{"639":"C 30"},{"571":"C 70"},{"573":"C 70 CABRIOLET"},{"574":"CROSS COUNTRY"},{"689":"POLAR"},{"575":"S 40"},{"576":"S 60"},{"577":"S 60 R"},{"690":"S 70"},{"578":"S 80"},{"844":"S 90"},{"581":"V 40"},{"1035":"V 40 CROSS COUNTRY"},{"583":"V 50"},{"921":"V 60"},{"584":"V 70"},{"585":"V 70 R"},{"845":"V 90"},{"780":"XC 60"},{"586":"XC 70"},{"587":"XC 90"}]';
        $this->createModele($mod, $id);

    }

    public function createModele($mod, $id)
    {
        $mod = json_decode($mod);
        foreach ($mod as $value) {
            $arr = get_object_vars($value);
            $nom = reset($arr);
            Modele::create(['nom' => $nom, 'marque_id' => $id]);
        }

    }
}
