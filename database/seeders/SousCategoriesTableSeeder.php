<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\SousCategory;
use Illuminate\Database\Seeder;

class SousCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1 Immobilier
        SousCategory::create(['id'=>'1','nom'=>'Appartements','category_id'=>'1','societe_id'=>'1']);
        SousCategory::create(['id'=>'2','nom'=>'Maisons et Villas','category_id'=>'1','societe_id'=>'1']);
        SousCategory::create(['id'=>'3','nom'=>'Locations de vacances','category_id'=>'1','societe_id'=>'1']);
        SousCategory::create(['id'=>'4','nom'=>'Bureaux et Plateaux','category_id'=>'1','societe_id'=>'1']);
        SousCategory::create(['id'=>'5','nom'=>'Magasins, Commerces et Locaux industriels','category_id'=>'1','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'Terrains et Fermes','category_id'=>'1','societe_id'=>'1']);
        SousCategory::create(['id'=>'7','nom'=>'Autre Immobilier','category_id'=>'1','societe_id'=>'1']);
        SousCategory::create(['id'=>'8','nom'=>'Colocations','category_id'=>'1','societe_id'=>'1']);
        //2 Véhicules
        SousCategory::create(['id'=>'9','nom'=>'Voitures','category_id'=>'2','societe_id'=>'1']);
        SousCategory::create(['id'=>'10','nom'=>'Motos','category_id'=>'2','societe_id'=>'1']);
        SousCategory::create(['id'=>'11','nom'=>'Pièces et Accessoires pour véhicules','category_id'=>'2','societe_id'=>'1']);
        SousCategory::create(['id'=>'12','nom'=>'Bateaux','category_id'=>'2','societe_id'=>'1']);
        SousCategory::create(['id'=>'13','nom'=>'Remorques et Caravanes','category_id'=>'2','societe_id'=>'1']);
        SousCategory::create(['id'=>'14','nom'=>'Engins Agricole','category_id'=>'2','societe_id'=>'1']);
        SousCategory::create(['id'=>'15','nom'=>'Engins BTP','category_id'=>'2','societe_id'=>'1']);
        SousCategory::create(['id'=>'16','nom'=>'Camions','category_id'=>'2','societe_id'=>'1']);
        // 3 Pour la Maison et Jardin
        SousCategory::create(['id'=>'17','nom'=>'Electroménager et Vaisselles','category_id'=>'3','societe_id'=>'1']);
        SousCategory::create(['id'=>'18','nom'=>'Meubles et Décoration','category_id'=>'3','societe_id'=>'1']);
        SousCategory::create(['id'=>'19','nom'=>'Jardin et Outils de bricolage','category_id'=>'3','societe_id'=>'1']);
        // 4 Loisirs et Divertissement
        SousCategory::create(['id'=>'20','nom'=>'Vélos','category_id'=>'4','societe_id'=>'1']);
        SousCategory::create(['id'=>'21','nom'=>'Sports et Loisirs','category_id'=>'4','societe_id'=>'1']);
        SousCategory::create(['id'=>'22','nom'=>'Animaux','category_id'=>'4','societe_id'=>'1']);
        SousCategory::create(['id'=>'23','nom'=>'Films, Livres, Magazines','category_id'=>'4','societe_id'=>'1']);
        SousCategory::create(['id'=>'24','nom'=>'Voyages et Billetterie','category_id'=>'4','societe_id'=>'1']);
        SousCategory::create(['id'=>'25','nom'=>'Art et Collections','category_id'=>'4','societe_id'=>'1']);
        SousCategory::create(['id'=>'26','nom'=>'Instruments de musique','category_id'=>'4','societe_id'=>'1']);
        // 5 Informatique et Multimedia
        SousCategory::create(['id'=>'27','nom'=>'Téléphones','category_id'=>'5','societe_id'=>'1']);
        SousCategory::create(['id'=>'28','nom'=>'Image & Son','category_id'=>'5','societe_id'=>'1']);
        SousCategory::create(['id'=>'29','nom'=>'Ordinateurs portables','category_id'=>'5','societe_id'=>'1']);
        SousCategory::create(['id'=>'30','nom'=>'Accessoires informatique et Gadgets','category_id'=>'5','societe_id'=>'1']);
        SousCategory::create(['id'=>'31','nom'=>'Jeux vidéo et Consoles','category_id'=>'5','societe_id'=>'1']);
        SousCategory::create(['id'=>'32','nom'=>'Appareils photo et Caméras','category_id'=>'5','societe_id'=>'1']);
        SousCategory::create(['id'=>'33','nom'=>'Tablettes','category_id'=>'5','societe_id'=>'1']);
        SousCategory::create(['id'=>'34','nom'=>'Télévisions','category_id'=>'5','societe_id'=>'1']);
        // 6 Emploi et Services
        SousCategory::create(['id'=>'35','nom'=>"Offres d'emploi",'category_id'=>'6','societe_id'=>'1']);
        SousCategory::create(['id'=>'36','nom'=>"Demandes d'emploi",'category_id'=>'6','societe_id'=>'1']);
        SousCategory::create(['id'=>'37','nom'=>'Services','category_id'=>'6','societe_id'=>'1']);
        SousCategory::create(['id'=>'38','nom'=>'Cours et Formations','category_id'=>'6','societe_id'=>'1']);
        // 7 Habillement et Bien Etre
        SousCategory::create(['id'=>'39','nom'=>'Vêtements','category_id'=>'7','societe_id'=>'1']);
        SousCategory::create(['id'=>'40','nom'=>'Chaussures','category_id'=>'7','societe_id'=>'1']);
        SousCategory::create(['id'=>'41','nom'=>'Montres et Bijoux','category_id'=>'7','societe_id'=>'1']);
        SousCategory::create(['id'=>'42','nom'=>'Sacs et Accessoires','category_id'=>'7','societe_id'=>'1']);
        SousCategory::create(['id'=>'43','nom'=>'Vêtements pour enfant et bébé','category_id'=>'7','societe_id'=>'1']);
        SousCategory::create(['id'=>'44','nom'=>'Equipements pour enfant et bébé','category_id'=>'7','societe_id'=>'1']);
        SousCategory::create(['id'=>'45','nom'=>'Produits de beauté','category_id'=>'7','societe_id'=>'1']);
        // 8 Entreprises
        SousCategory::create(['id'=>'46','nom'=>'Business et Affaires commerciales','category_id'=>'8','societe_id'=>'1']);
        SousCategory::create(['id'=>'47','nom'=>'Matériels Professionnels','category_id'=>'8','societe_id'=>'1']);
        SousCategory::create(['id'=>'48','nom'=>'Stocks et Vente en gros','category_id'=>'8','societe_id'=>'1']);

/*        SousCategory::create(['id'=>'49','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'50','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'51','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'52','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'53','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'54','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'55','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'56','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'57','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'58','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'59','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);
        SousCategory::create(['id'=>'6','nom'=>'','category_id'=>'','societe_id'=>'1']);*/
    }
}
