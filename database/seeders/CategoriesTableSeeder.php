<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['id'=>'1','nom'=>'Immobilier','societe_id'=>'1']);
        Category::create(['id'=>'2','nom'=>'Véhicules','societe_id'=>'1']);
        Category::create(['id'=>'3','nom'=>'Pour la Maison et Jardin','societe_id'=>'1']);
        Category::create(['id'=>'4','nom'=>'Loisirs et Divertissement','societe_id'=>'1']);
        Category::create(['id'=>'5','nom'=>'Informatique et Multimedia','societe_id'=>'1']);
        Category::create(['id'=>'6','nom'=>'Emploi et Services','societe_id'=>'1']);
        Category::create(['id'=>'7','nom'=>'Habillement et Bien Etre','societe_id'=>'1']);
        Category::create(['id'=>'8','nom'=>'Entreprises','societe_id'=>'1']);
    }
}
