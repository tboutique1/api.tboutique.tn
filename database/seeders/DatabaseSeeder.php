<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
/*        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);*/
//        $this->call(GouvernoratsTableSeeder::class);
        $this->call(MarqueVoituresTableSeeder::class);
//        $this->call(DelegationsTableSeeder::class);
/*        $this->call(CategoriesTableSeeder::class);
        $this->call(SousCategoriesTableSeeder::class);*/

    }
}
