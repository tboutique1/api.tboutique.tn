<?php

namespace Database\Seeders;

use App\Models\Localite;
use Illuminate\Database\Seeder;

class LocalitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Localite::create(['id'=>'','nom'=>'Ariana','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Borj El Baccouche','code_postal'=>'2027','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Centre Commercial Ikram','code_postal'=>'2037','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Belvedere 2','code_postal'=>'2091','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Borj Turki 1','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Borj Turki 2','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite De La Sante','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Des Roses','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Du Jardin','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Du Printemps','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Du Soleil','code_postal'=>'2091','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite El Intissar 1','code_postal'=>'2091','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite El Intissar 2','code_postal'=>'2091','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Ennasr 1','code_postal'=>'2037','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Ennasr 2','code_postal'=>'2037','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Ennouzha','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Essaada (Ariana)','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Essaada (Riadh Andal)','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Jaafar','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Jamil','code_postal'=>'2091','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Karim','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Cite Mehrzia 1','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'El Menzah 5','code_postal'=>'2091','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'El Menzah 6','code_postal'=>'2091','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'El Menzah 7','code_postal'=>'2037','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'El Menzah 8','code_postal'=>'2037','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Nouvelle Ariana','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Residence El Ouns','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Residence Ennour ( Naser','code_postal'=>'2037','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Residence Ennour (Ariana)','code_postal'=>'2080','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Residence Ibn Zeidoun','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Residence Ichbilia','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Residence Kortoba','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'Riadh Landlous','code_postal'=>'2058','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'','code_postal'=>'','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'','code_postal'=>'','delegation_id'=>'1']);
        Localite::create(['id'=>'','nom'=>'','code_postal'=>'','delegation_id'=>'1']);














    }
}
