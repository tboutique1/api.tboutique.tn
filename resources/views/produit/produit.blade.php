<meta property="fb:app_id" content="1034270743779701"/>
<meta property="og:url"
      content="http://api.tboutique.tn/produit/{{$newProduit['id']}}"/>
<meta property="og:type" content="og:product"/>
<meta property="og:title" content="{{$newProduit['titre']}} (prix: {{$newProduit['prix']}} TND)"/>
<meta property="og:description" content="{{$newProduit['description']}}"/>
<meta property="og:image"
      content="{{$newProduit['image_path']}}"/>
<meta property="og:image:alt" content="{{$newProduit['image_name']}}" />
<meta property="og:image:width" content="300" />
<meta property="og:image:height" content="300" />

<meta property="og:locale" content="ar_AR" />
<meta property="product:price:amount" content="{{$newProduit['prix']}}" />
<meta property="product:price:currency" content="TND" />
<meta property="og: site_name" content="tboutique.tn" />



<script>
    var url = "https://tboutique.tn/newProduit/view/" + {{$newProduit['id']}};
    location.replace(url);
</script>
