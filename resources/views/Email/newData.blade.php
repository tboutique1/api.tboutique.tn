@component('mail::message')
    new data:
    <ul>
        <li>
            {{$key}} => <b>{{$value}}</b>
        </li>
    </ul>
@endcomponent
