@component('mail::message')
Rappel tache:
<ul>
    <li>
        <a href="{{$url}}">{{$titre}}</a>
    </li>
</ul>
@endcomponent
