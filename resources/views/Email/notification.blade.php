@component('mail::message')
Nouveau produit ajouté:
<ul>
    <li>
        <a href="{{$url}}">{{$titre}}</a>
    </li>
</ul>
@endcomponent
