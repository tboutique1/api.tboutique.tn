<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'tboutique')
<img src="https://tboutique.tn/assets/img/favicons/logo.png" class="logo" alt="tboutique Logo" style="height: 90px !important; width: 150px;!important">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
